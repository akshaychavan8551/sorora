import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Splash from './src/components/Splash'
import Dashboard from './src/components/Dashboard'
import Login from './src/components/Login'

class App extends React.Component {
  render() {
    return (
        <AppContainer/>
    );
  }
}

const AppNavigator = createStackNavigator({
  Splash : { screen: Splash },
  Dashboard : { screen : Dashboard },
  Login : { screen : Login }
});

const AppContainer =  createAppContainer(AppNavigator);

export default App;

