import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import { StyleSheet, StatusBar, View, Image, Text, TouchableOpacity, ScrollView, Linking } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import {  LoginManager } from 'react-native-fbsdk';
import { GoogleSignin } from '@react-native-community/google-signin';
import Share from 'react-native-share';


class Profile extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
            username: '',
            email: '',
            usermobile: '',
            userID: '',
            userAddress: '',
            userGender: '',
            userProfile: '',
        }
    }
    shareSingleLink = async () => {
        const shareOptions = {
          title: 'Share Link',
          url: 'http://www.omsoftware.org/sorora/aboutus',
          failOnCancel: false,
        };
    
        try {
          const ShareResponse = await Share.open(shareOptions);
          setResult(JSON.stringify(ShareResponse, null, 2));
        } catch (error) {
          console.log('Error =>', error);
          setResult('error: '.concat(getErrorString(error)));
        }
      };
    componentDidMount() {
        setInterval(() => this.getLoggedUserData(), 5000);
    }
    getLoggedUserData = async () => {
        const username = await AsyncStorage.getItem('user_name');
        const useremail = await AsyncStorage.getItem('user_email');
        const usermobile = await AsyncStorage.getItem('user_mobile');
        const userId = await AsyncStorage.getItem('userID');
        const userAddress = await AsyncStorage.getItem('user_Address');
        const userGender = await AsyncStorage.getItem('user_Gender');
        const userProfile = await AsyncStorage.getItem('user_Profile');
        if (username !== null) {
            this.setState({
                username: username
            })
        }
        if (useremail !== null) {
            this.setState({
                email: useremail
            })
        }
        if (usermobile !== null) {
            this.setState({
                usermobile: usermobile
            })
        }
        if (userId !== null) {
            this.setState({
                userID: userId
            })
        }
        if (userAddress !== null) {
            this.setState({
                userAddress: userAddress
            })
        }
        if (userGender !== null) {
            this.setState({
                userGender: userGender
            })
        }
        if (userProfile !== null) {
            this.setState({
                userProfile: userProfile
            })
        }
        // console.log(this.state.username);  console.log(this.state.email); console.log(this.state.usermobile); console.log(this.state.userID)
        // console.log(this.state.userAddress); console.log(this.state.userGender);


    }
    logoutFunction = async () => {
        LoginManager.logOut();
        try {
            await GoogleSignin.signOut();
        } catch (error) {
            console.log('google sign out error in login',error);
        }

        await AsyncStorage.removeItem('user_name');
        await AsyncStorage.removeItem('user_email');
        await AsyncStorage.removeItem('userID');
        await AsyncStorage.removeItem('user_mobile');
        await AsyncStorage.removeItem('user_Address');
        await AsyncStorage.removeItem('user_Gender');
        await AsyncStorage.removeItem('user_Profile');
    
        await AsyncStorage.removeItem('provider_id')
        await AsyncStorage.removeItem('subcategory_id')
        await AsyncStorage.removeItem('category_id')

        this.setState({
            username: '',
            email: '',
            usermobile: '',
            userID: '',
            userAddress: '',
            userGender: ''
        })

        this.props.navigation.navigate('Search');
    }
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <View style={styles.headerView}>
                    <Text style={styles.headerText}>My Profile</Text>
                </View>
                <ScrollView style={styles.bodyView}>
                    {(this.state.username != '' && this.state.useremail != '' && this.state.usermobile != '') ?
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CreateProfile')}>
                            <View style={styles.loggedProfileBtnView}>
                                <View style={{ width: '20%' }}>
                                    {(this.state.userProfile == '')?
                                        <Icon name='user-circle-o' size={50} color='#ccc' />
                                        :
                                        <Image
                                            source={{ uri: this.state.userProfile }}
                                            style={{ height: 50, width: 50, borderRadius: 25}}
                                        />
                                    }   
                                </View>
                                <View style={{ width: '72%' }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 14 }}>{this.state.username}</Text>
                                    <Text style={{ fontSize: 14 }}>{this.state.email}</Text>
                                    <Text style={{ fontSize: 14 }}>{this.state.usermobile}</Text>
                                </View>
                                <View style={styles.chevronRightIcon}>
                                    <Icon name='chevron-right' color='#000' />
                                </View>
                            </View>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                            <View style={styles.profileBtnView}>
                                <View style={{ width: '90%' }}>
                                    <Text>Login / Signup</Text>
                                </View>
                                <View style={styles.chevronRightIcon}>
                                    <Icon name='chevron-right' color='#000' />
                                </View>
                            </View>
                        </TouchableOpacity>
                    }
                    <TouchableOpacity onPress={() => Linking.openURL('https://omsoftware.org/sorora/serviceProvider')}>
                        <View style={styles.profileBtnView}>
                            <View style={{ width: '90%' }}>
                                <Text>Register As Partner</Text>
                            </View>
                            <View style={styles.chevronRightIcon}>
                                <Icon name='chevron-right' color='#000' />
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => Linking.openURL('http://www.omsoftware.org/sorora/aboutus')}>
                        <View style={styles.profileBtnView}>
                            <View style={{ width: '90%' }}>
                                <Text>About Sorora</Text>
                            </View>
                            <View style={styles.chevronRightIcon}>
                                <Icon name='chevron-right' color='#000' />
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.shareSingleLink()}>
                        <View style={styles.profileBtnView}>
                            <View style={{ width: '90%' }}>
                                <Text>Share Sorora</Text>
                            </View>
                            <View style={styles.chevronRightIcon}>
                                <Icon name='chevron-right' color='#000' />
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View>
                        {(this.state.username != '' && this.state.useremail != '' && this.state.usermobile != '') ?
                            <TouchableOpacity onPress={()=> this.props.navigation.navigate('RecentlySearch')}>
                                <View style={styles.profileBtnView}>
                                    <View style={{ width: '90%' }}>
                                        <Text>Recently Search</Text>
                                    </View>
                                    <View style={styles.chevronRightIcon}>
                                        <Icon name='chevron-right' color='#000' />
                                    </View>
                                </View>
                            </TouchableOpacity>
                            :
                            null
                        }
                    </View>
                    <View>
                        {(this.state.username != '' && this.state.useremail != '' && this.state.usermobile != '') ?
                            <TouchableOpacity onPress={()=> this.props.navigation.navigate('TermsAndConditions')}>
                                <View style={styles.profileBtnView}>
                                    <View style={{ width: '90%' }}>
                                        <Text>Terms {'\&'} Condition</Text>
                                    </View>
                                    <View style={styles.chevronRightIcon}>
                                        <Icon name='chevron-right' color='#000' />
                                    </View>
                                </View>
                            </TouchableOpacity>
                            :
                            null
                        }
                    </View>
                    <View>
                        {(this.state.username != '' && this.state.useremail != '' && this.state.usermobile != '') ?
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('CallToOrder')}>
                                <View style={styles.profileBtnView}>
                                    <View style={{ width: '90%' }}>
                                        <Text>Call To Order</Text>
                                    </View>
                                    <View style={styles.chevronRightIcon}>
                                        <Icon name='chevron-right' color='#000' />
                                    </View>
                                </View>
                            </TouchableOpacity>
                            :
                            null
                        }
                    </View>
                    <View>
                        {(this.state.username != '' && this.state.useremail != '' && this.state.usermobile != '') ?
                            <TouchableOpacity onPress={() => this.logoutFunction()}>
                                <View style={styles.profileBtnView}>
                                    <View style={{ width: '90%' }}>
                                        <Text>Logout</Text>
                                    </View>
                                    <View style={styles.chevronRightIcon}>
                                        <Icon name='chevron-right' color='#000' />
                                    </View>
                                </View>
                            </TouchableOpacity>
                            :
                            null
                        }
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7'
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
    },
    bodyView: {
        marginVertical: 15,
        marginHorizontal: 15,
        height: '70%'
    },
    profileBtnView: {
        backgroundColor: '#fff',
        height: 45,
        width: '100%',
        borderRadius: 15,
        borderWidth: 1,
        borderColor: '#ccc',
        flexDirection: 'row',
        position: 'relative',
        alignItems: 'center',
        paddingLeft: 10,
        marginBottom: 10
    },
    loggedProfileBtnView: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        //height: 50,
        width: '100%',
        borderRadius: 15,
        borderWidth: 1,
        borderColor: '#ccc',
        flexDirection: 'row',
        position: 'relative',
        alignItems: 'center',
        padding: 10,
        marginBottom: 20
    },
    chevronRightIcon: {
        width: '10%'
        //right: -180,
    },
});

export default withNavigation(Profile);