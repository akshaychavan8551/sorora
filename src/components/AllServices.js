import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, ScrollView, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';

export default class AllServices extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
            categoryData: [],
            categoryID: '',
            subCategoryData: [],
            spinner: false,
        }
    }
    componentDidMount() {
        this.getCategoryData();
        this.getCategoryID();
        this.getSubCategoryData();
    }
    getCategoryID = async() => {
        let catID = JSON.stringify(this.props.navigation.getParam('categoryId', 'NO-ID'))
        this.setState({ categoryID: catID })

        const catIDS = await AsyncStorage.getItem('category_ids');
        console.log(catIDS)
    }
    getCategoryData = () => {
        this.setState({
            spinner: !this.state.spinner
        });
        return fetch('https://www.omsoftware.org/sorora/api/getAllCategories')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({ categoryData: responseJson.CATEGORY_DATA, spinner: false });
            })
            .catch((error) => {
                this.setState({spinner: false })
                console.log(error)
            //     Alert.alert(
            //         '',
            //         'Unable to process this request. We apologize for any inconvenience. Please try again later.',
            //         [
            //             { text: 'OK', onPress: () => this.setState({ spinner: false }) },
            //         ]
            //     )
            // }).done();
            });
    }
    getSubCategoryData = () => {
        this.setState({
            spinner: !this.state.spinner
        });
        let catID = this.props.navigation.getParam('categoryId', 'NO-ID')
        // let fd = new FormData();
        // fd.append('category_id', catID);
        // let data ={
        //     'category_id': catID
        // }
        var apiHeader = {
            method: 'POST',
            headers: {
                // Accept: 'application/json',
                "Content-Type": "application/json",
                "Connection": "keep-alive",
                "Content-Type": "application/x-www-form-urlencoded",
                "accept-encoding": "gzip,deflate",
                "cache-control": "no-cache,no-cache"
            }, 
            body: "category_id="+ catID         
        }
        fetch('https://omsoftware.org/sorora/api/getSubCategoryByCategoryId', apiHeader
        ).then((response) => response.json())
            .then((responseJson) => {
                this.setState({ subCategoryData: responseJson.SUBCATEGORY_DETAILS, spinner: false });
            })
            .catch((error) => {
                this.setState({spinner: false })
                console.log(error)
                Alert.alert(
                    '',
                    'Server error. Please try again later.',
                    [
                        { text: 'OK', onPress: () => this.setState({ spinner: false }) },
                    ]
                )
            }).done();
            // });
    }
    showSubCategoryData = async(category_id, cat_name) => {
        await AsyncStorage.setItem('category_name', cat_name)
        this.setState({
            spinner: !this.state.spinner
        });
        // let fd = new FormData();
        // fd.append('category_id', category_id);
        // let catID = JSON.stringify(category_id)
        var apiHeader = {
                method: 'POST',
                headers: {
                    // Accept: 'application/json',
                    "Content-Type": "application/json",
                    "Connection": "keep-alive",
                    "Content-Type": "application/x-www-form-urlencoded",
                    "accept-encoding": "gzip,deflate",
                    "cache-control": "no-cache,no-cache"
                }, 
                body: "category_id="+ category_id         
        }
        fetch('https://omsoftware.org/sorora/api/getSubCategoryByCategoryId',apiHeader
        ).then((response) => response.json())
            .then((responseJson) => {
                this.setState({ subCategoryData: responseJson.SUBCATEGORY_DETAILS, spinner: false });
                this.setState({ categoryID: category_id });
            })
            .catch((error) => {
                this.setState({spinner: false })
                console.log(error)
                Alert.alert(
                    '',
                    'Server Error. Please try again later.',
                    [
                        { text: 'OK', onPress: () => this.setState({ spinner: false }) },
                    ]
                )
            }).done();
            // });
    }
    goBack = async() => {
        await AsyncStorage.removeItem('category_name')
        this.setState({ categoryID: '' })
        this.props.navigation.goBack()
    } 
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                <View style={styles.headerView}>
                    <View style={styles.backBtnView}>
                        <TouchableOpacity style={{ height: '100%', width: '100%'}} onPress={() => this.goBack()}>
                            <Image
                                source={require('../assets/images/left_arrow.png')}
                                style={{ height: 25, width: 25 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.allServiceTitleView}>
                        <Text style={styles.headerText}>All Services</Text>
                    </View>
                    <View style={styles.searchIconView}>
                        {/* <Icon name='search' color='#fff' size={20} /> */}
                    </View>
                </View>
                <View>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} bounces={false}>
                        {this.state.categoryData.map((data, index) => {
                            return (
                                <View style={styles.categoryScrollview}>
                                    {(data.category_id == this.state.categoryID) ?
                                        <TouchableOpacity onPress={() => this.showSubCategoryData(data.category_id, data.category_name)}>
                                            <Text style={styles.activeCategoryText}>{data.category_name}</Text>
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity onPress={() => this.showSubCategoryData(data.category_id, data.category_name)}>
                                            <Text style={{ fontSize: 16 }}>{data.category_name}</Text>
                                        </TouchableOpacity>
                                    }
                                </View>
                            )
                        })
                        }
                    </ScrollView>
                </View>
                <ScrollView style={{ height: '50%' }}>
                    {
                        this.state.subCategoryData.map((data, index) => {
                            return (
                                <View>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Professionals', { subcategory_id: data.subcategory_id, subcategory_name: data.subcategory_name })}>
                                        <View style={styles.subcategoryView}>
                                            <View style={{ width: '25%', alignSelf: 'center' }}>
                                                <View style={styles.subcatImgView}>
                                                    <Image
                                                        source={{ uri: data.url }}
                                                        style={styles.subcatImg}
                                                    />
                                                </View>
                                            </View>
                                            <View style={styles.subcatNameView}>
                                                <Text style={{ fontSize: 16 }}>{data.subcategory_name}</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            )
                        })
                    }
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7',
        height: '100%',
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
    },
    backBtnView: {
        width: '20%',
        padding: 10,
        alignItems: 'flex-start'
    },
    allServiceTitleView: {
        width: '50%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    searchIconView: {
        width: '30%',
        padding: 10,
        alignItems: 'flex-end',
    },
    categoryScrollview: {
        backgroundColor: '#fff',
        height: 40,
        padding: 10,
        marginBottom: 20
    },
    activeCategoryText: {
        fontSize: 16,
        borderBottomColor: '#91d242',
        borderBottomWidth: 4,
        paddingBottom: 5
    },
    subcategoryView: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        margin: 5,
        padding: 5,
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 10
    },
    subcatImgView: {
        height: 50,
        width: 50,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: '#ccc',
        alignItems: 'center',
        justifyContent: 'center'
    },
    subcatImg: {
        height: 30,
        width: 30,
        alignSelf: 'center'
    },
    subcatNameView: {
        width: '50%',
        alignItems: 'flex-start'
    }

});            