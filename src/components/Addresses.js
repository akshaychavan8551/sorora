import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, ToastAndroid, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { CheckBox } from 'react-native-elements'
import AsyncStorage from '@react-native-community/async-storage';
// import Spinner from 'react-native-loading-spinner-overlay';

// import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button'

export default class Addresses extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
            // loggedID: '',
            // spinner: false,
            addressData: [],
            checked: false,
            user_addressId: '',

            address: '',
            landmark: '',

            // 
            provider_id: '',
            subcategory_id: '',
            category_id: '',
            service_id: '',
        }
    }
    componentDidMount() {
        this.setState({ user_addressId: ''})
        this.getUserAddress();
        setInterval(() => this.getUserAddress(), 3000);
        this.getData();
    }
    getData = () => {
        // let provider = JSON.stringify(this.props.navigation.getParam('provider_id', 'NO-ID'))
        // let provider_id = provider.replace(/"/g, "");

        // let subcategory = JSON.stringify(this.props.navigation.getParam('subcategory_id', 'NO-ID'))
        // let subcategory_id = subcategory.replace(/"/g, "");

        // let category = JSON.stringify(this.props.navigation.getParam('category_id', 'NO-ID'))
        // let category_id = category.replace(/"/g, "");

        let serviceid = JSON.stringify(this.props.navigation.getParam('s_id', 'NO-ID'))
        let serv_id = serviceid.replace(/"/g, "");

        // this.setState({ provider_id: provider_id })
        // this.setState({ subcategory_id: subcategory_id })
        // this.setState({ category_id: category_id })
        this.setState({ service_id: serv_id })

        console.log('addresses : serv_id: ' + serv_id)
    }
    getUserAddress = () => {
        // this.setState({
        //     spinner: !this.state.spinner
        // });
        let userId = this.props.navigation.getParam('user_id', 'NO-ID')
        // let userId = JSON.stringify(this.props.navigation.getParam('user_id', 'NO-ID'))
        // let user_Id = userId.replace(/"/g, "");
        // // console.log('id'+ userId)
        // let fd = new FormData();
        // fd.append('user_id', user_Id);
        var apiHeader={
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Connection": "keep-alive",
                "Content-Type": "application/x-www-form-urlencoded",
                "accept-encoding": "gzip,deflate",
                "cache-control": "no-cache,no-cache"
            },
            body: "user_id="+userId
        }
        fetch('https://omsoftware.org/sorora/api/getAddressByUserId', apiHeader
        ).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.RESPONSECODE == 0) {
                    Alert.alert(responseJson.RESPONSE)
                } else if (responseJson.RESPONSECODE == 1) {
                    this.setState({ addressData: responseJson.Data, spinner: false })

                } else {
                    console.log(responseJson)
                }
            })
            .catch((error) => {
                this.setState({ spinner: false })
                console.log(error)
            //     Alert.alert(
            //         '',
            //         'Unable to process this request. We apologize for any inconvenience. Please try again later.',
            //         [
            //             { text: 'OK' },
            //         ]
            //     )
            // }).done();
            });

    }
    setAddressToNext = (id, address, landmark) => {
        if(this.state.user_addressId === id){
            this.setState({ user_addressId: '' })
        }else{
            console.log('address Id : ' + id)
            this.setState({ user_addressId: id })
            this.setState({ address: address })
            this.setState({ landmark: landmark })
        // alert(id)
        }
    }
    deleteUserAddress = (id) => {
        // let fd = new FormData();
        // fd.append('address_id', id);
        var apiHeader = {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Connection": "keep-alive",
                "Content-Type": "application/x-www-form-urlencoded",
                "accept-encoding": "gzip,deflate",
                "cache-control": "no-cache,no-cache"
            },
            body: "address_id="+id
        }
        fetch('https://omsoftware.org/sorora/api/userdeleteAddres', apiHeader
        ).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.RESPONSECODE == 0) {
                    Alert.alert(responseJson.RESPONSE)
                } else if (responseJson.RESPONSECODE == 1) {
                    this.setState({ user_addressId: ''})
                    Alert.alert(responseJson.RESPONSE)
                    this.props.navigation.navigate('Addresses');

                } else {
                    console.log(responseJson)
                }
            })
            .catch((error) => {
                console.log(error)
            //     Alert.alert(
            //         '',
            //         'Unable to process this request. We apologize for any inconvenience. Please try again later.',
            //         [
            //             { text: 'OK' },
            //         ]
            //     )
            // }).done();
            });
    }
    goCustomAddrsForEdit = (address_id, address, landmark, locality) => {
        // if(this.state.user_addressId != ''){
            this.setState({ user_addressId: ''})
            let userId = JSON.stringify(this.props.navigation.getParam('user_id', 'NO-ID'))
            let user_Id = userId.replace(/"/g, "");
            console.log('user id ' + user_Id);
            console.log('addrs id ' + address_id);
            console.log('addrs ' + address);
            console.log('land ' + landmark);
            console.log('loca ' + locality);
            this.props.navigation.navigate('EditAddress', { user_id: user_Id, address_id: address_id, address: address, landmark: landmark, locality: locality })

        // }else{
        //     Alert.alert('Please select address')
        // }

        

    }
    goToNextProcess = async () => {
        let provider = JSON.stringify(this.props.navigation.getParam('provider_id', 'NO-ID'))
        let provider_id = provider.replace(/"/g, "");

        let subcategory = JSON.stringify(this.props.navigation.getParam('subcategory_id', 'NO-ID'))
        let subcategory_id = subcategory.replace(/"/g, "");

        let category = JSON.stringify(this.props.navigation.getParam('category_id', 'NO-ID'))
        let category_id = category.replace(/"/g, "");

        const userId = await AsyncStorage.getItem('userID');

        if (this.state.user_addressId != '') {
            await AsyncStorage.setItem('provider_id', provider_id)
            await AsyncStorage.setItem('subcategory_id', subcategory_id)
            await AsyncStorage.setItem('category_id', category_id)
            await AsyncStorage.setItem('s_id',this.state.service_id)
            await AsyncStorage.setItem('address', this.state.address)
            await AsyncStorage.setItem('landmark',this.state.landmark)
           
            this.props.navigation.navigate('ServiceList');
            // this.props.navigation.navigate('ServiceList', { provider_id: provider_id, subcategory_id: subcategory_id, category_id: category_id, user_id: userId, s_id: this.state.service_id, address: this.state.address, landmark: this.state.landmark });
        } else {
            Alert.alert('Please select address')
        }
        console.log(' address page : this.state.provider_id' + this.state.provider_id + 'this.state.subcategory_id' + this.state.subcategory_id + 'this.state.category_id' + this.state.category_id)
    }
    goSearchPage = async() => {
        await AsyncStorage.removeItem('category_name')
        await AsyncStorage.removeItem('serviceName')

        await AsyncStorage.removeItem('provider_id');
        await AsyncStorage.removeItem('subcategory_id');
        await AsyncStorage.removeItem('category_id');
        await AsyncStorage.removeItem('s_id')
        await AsyncStorage.removeItem('address')
        await AsyncStorage.removeItem('landmark')
        this.props.navigation.navigate('Search')
    }
    goAddLocation = () =>{
        this.setState({ user_addressId: ''})
        this.props.navigation.navigate('CutomerAddress', { user_id: this.state.loggedID })
    }
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                {/* <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                /> */}
                <View style={styles.headerView}>
                    <View style={styles.backBtnView}>
                        <TouchableOpacity style={{ height: '100%', width: '100%'}} onPress={() => this.goSearchPage()}>
                            <Icon name='home' color='#fff' size={25} />
                            {/* <Image
                                source={require('../assets/images/left_arrow.png')}
                                style={{ height: 25, width: 25 }}
                            /> */}
                        </TouchableOpacity>
                    </View>
                    <View style={styles.professionalTitleView}>
                        <Text style={styles.headerText}>Address</Text>
                    </View>
                </View>
                <View style={{ alignSelf: 'flex-end', padding: 15 }}>
                    <TouchableOpacity onPress={() => this.goAddLocation()}>
                        <View>
                            <Icon name='plus' size={40} color='#89cf34' />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{ margin: 10 }}>
                    {this.state.addressData.map((data, index) => {
                        return (
                            <View style={{ backgroundColor: '#fff', width: '100%', borderWidth: 0.5, borderColor: '#ccc', padding: 10, marginBottom: 10 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ marginHorizontal: 12 }}>
                                        {/* this.state.user_addressId === data.address_id  */}
                                        {(this.state.user_addressId != '' && this.state.user_addressId === data.address_id ) ?
                                            <TouchableOpacity onPress={() => this.setAddressToNext(data.address_id, data.address, data.landmark)}>
                                                <Icon name='check-square-o' color='green' size={30} />
                                            </TouchableOpacity>

                                            :
                                            <TouchableOpacity onPress={() => this.setAddressToNext(data.address_id, data.address, data.landmark)}>
                                                <Icon name='square-o' color='#000' size={30} />
                                            </TouchableOpacity>
                                        }
                                    </View>
                                    <View style={{ width: '100%', padding: 5}}>
                                        <TouchableOpacity  onPress={() => this.setAddressToNext(data.address_id, data.address, data.landmark)}>
                                            <Text style={{ width: '95%',fontSize: 15, fontWeight: '400' }}>{data.address}</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity  onPress={() => this.setAddressToNext(data.address_id, data.address, data.landmark)}>
                                            <Text style={{ fontSize: 16, fontWeight: '400' }}>{data.landmark}</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity  onPress={() => this.setAddressToNext(data.address_id, data.address, data.landmark)}>
                                            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{data.locality}</Text>
                                        </TouchableOpacity> 
                                         
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                                    <View>
                                        <TouchableOpacity onPress={() => this.goCustomAddrsForEdit(data.address_id, data.address, data.landmark, data.locality)}>
                                            <View style={{ backgroundColor: '#89cf34', alignItems: 'center', justifyContent: 'center', height: 40, width: 100, margin: 5 }}>
                                                <Text style={{ color: '#fff', fontSize: 14 }}>Edit</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <TouchableOpacity onPress={() => this.deleteUserAddress(data.address_id)}>
                                            <View style={{ backgroundColor: '#89cf34', alignItems: 'center', justifyContent: 'center', height: 40, width: 100, margin: 5 }}>
                                                <Text style={{ color: '#fff', fontSize: 14 }}>Delete</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        )
                    })
                    }
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center', marginBottom: 5 }}>
                    <TouchableOpacity onPress={() => this.goToNextProcess()}>
                        <View style={styles.nextBtnView}>
                            <Text style={styles.nextBtnText}>NEXT</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7',
        height: '100%',
    },
    // spinnerTextStyle: {
    //     color: '#FFF'
    // },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
    },
    backBtnView: {
        width: '20%',
        padding: 10,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    professionalTitleView: {
        width: '80%',
        paddingLeft: 60,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    addressFormView: {
        padding: 15,
        width: '100%'
    },
    addressTitleText: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    nextBtnView: {
        backgroundColor: '#89cf34',
        height: 50,
        width: 150,
        alignItems: 'center',
        justifyContent: 'center'
    },
    nextBtnText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16
    }
});