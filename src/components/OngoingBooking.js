import React, { Component } from 'react';
import { StyleSheet, StatusBar, View, Text, ScrollView, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

export default class OngoingBooking extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
            message: '',
            bookingData: []
        }
    }
    componentDidMount() {
        setInterval(() => this.getOnGoingServices(), 4000);
        // this.getOnGoingServices()
    }
    getOnGoingServices = async () => {
        let userId = await AsyncStorage.getItem('userID')
        if(userId == null){
            this.setState({ bookingData: []})
        }else{
            let fd = new FormData();
            fd.append('user_id', userId);
            fetch('https://omsoftware.org/sorora/api/userOnGoingServiceList', {
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                body: fd
            }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.RESPONSECODE == 0) {
                        this.setState({ message: responseJson.RESPONSE })
                    } else if (responseJson.RESPONSECODE == 1) {
                        this.setState({ bookingData: responseJson.Data })
                        // console.log(responseJson)
                        // console.log(responseJson)
                    } else {
                        console.log(responseJson)
                    }
                })
                .catch((error) => {
                    console.log(error)
                })
        }

    }

    cancelBookingByUser = (booking_id) => {
        console.log(booking_id)
        let fd = new FormData();
        fd.append('booking_id', booking_id);
        fetch('https://omsoftware.org/sorora/api/bookingCancelByUser', {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: fd
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.RESPONSECODE == 0) {
                    alert(responseJson.RESPONSE)
                } else if (responseJson.RESPONSECODE == 1) {
                    alert(responseJson.RESPONSE)
                    // console.log(responseJson)
                } else {
                    console.log(responseJson)
                }
            })
            .catch((error) => {
                console.log(error)
            })
    }
    render() {
        return (
            <ScrollView style={styles.mainView}>
                <View>
                    {
                        this.state.bookingData.map((data) => {
                            return (
                                <View style={{ borderColor: '#ccc', borderWidth: 1, padding: 5, margin: 10 }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('OngoingServiceDetails', { booking_id: data.booking_id })}>
                                        <View>
                                            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{data.subcategory_name}</Text>
                                            <Text style={{ fontWeight: 'bold' }}>Date : &nbsp; <Text style={{ fontSize: 14, fontWeight: '400' }}>{data.available_date} {data.available_time}</Text></Text>
                                            <Text style={{ fontWeight: 'bold' }}>Total : &nbsp;{data.s_currency}&nbsp;{data.price}</Text>
                                            {(data.start_time != '0000-00-00 00:00:00') ?
                                                <Text style={{ fontWeight: 'bold', color: 'orange' }}>Status :  <Text>Started</Text></Text>
                                                :
                                                data.booking_status == 0 ?
                                                    <Text style={{ fontWeight: 'bold', color: 'orange' }}>Status :  <Text>Pending</Text></Text>
                                                    :
                                                    <Text style={{ fontWeight: 'bold', color: 'green' }}>Status :  <Text>Accepted</Text></Text>
                                            }
                                        </View>
                                    </TouchableOpacity>
                                    <View style={{ alignSelf: 'flex-end' }}>
                                        <TouchableOpacity onPress={() => this.cancelBookingByUser(data.booking_id)}>
                                            <View style={{ backgroundColor: 'red', height: 30, width: 60, alignItems: 'center', justifyContent: 'center', margin: 5, borderRadius: 5 }}>
                                                <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 14 }}>Cancel</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        })
                    }
                </View>
                {(this.state.bookingData.length === 0) ?
                    <View style={{ flex: 1, marginTop: '60%' }}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', textAlign: 'center' }}>No Record Found..!</Text>
                    </View>
                    :
                    null
                }
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor: '#f7f7f7',
        height: '90%',
        width: '100%'
    },
});