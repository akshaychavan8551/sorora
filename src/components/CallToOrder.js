import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, Linking } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class CallToOrder extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {}
    }
    render() {
        return(
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <View style={styles.headerView}>
                    <View style={styles.backBtnView}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image
                                source={require('../assets/images/left_arrow.png')}
                                style={{ height: 25, width: 25 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.professionalTitleView}>
                        <Text style={styles.headerText}>Call To Order</Text>
                    </View>
                </View>
                <View style={{ padding: 15, width: '100%'}}>
                    <View style={{ alignItems: 'center', marginBottom: 10}}>
                        <Image
                            source={require('../assets/images/ic_call_to_order.png')}
                            style={{ height: 150, width: 150 }}
                        />
                    </View> 
                    <View style={{ marginBottom: 20, marginTop: 30}}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', marginVertical: 10, textAlign: 'center'}}>ORDER BY PHONE</Text>
                        <Text style={{ fontSize: 14, textAlign: 'center', fontWeight: 'normal'}}>Need help place an order,{'\n'}call our customer line:</Text>
                    </View> 
                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginBottom: 20}}>
                        <Icon name='phone-square' size={25} color='#007a68'/>
                        <Text>&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                        <Text style={{ color: '#007a68', fontWeight: 'bold', fontSize: 18}}>+91 1234567890</Text>
                    </View>  
                    <View style={{flexDirection: 'row', justifyContent: 'center', marginBottom: 20}}>
                        <Icon name='calendar' size={20} color='#000'/>
                        <Text>&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 16}}>OPERATION HOURS</Text>
                    </View> 
                    <View style={{ alignItems: 'center'}}>
                        <Text style={{ fontSize: 14, marginVertical: 5 }}>Monday - Friday  8 am - 5 pm</Text>
                        <Text style={{ fontSize: 14, marginVertical: 5 }}>Saturday  9 am - 3 pm</Text>
                        <Text style={{ fontSize: 14, marginVertical: 5 }}>Public Holidays 9 am - 3 pm</Text>
                    </View>               
                </View> 
                <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end', margin: 10 }}>
                    <View style={{ backgroundColor: '#89cf34', alignItems: 'center', justifyContent: 'center', height: 45, width: '100%'}}>
                        <TouchableOpacity onPress={()=> Linking.openURL(`tel: +91 1234567890`)} style={{ width: '100%', height: '100%', alignSelf: 'center',justifyContent: 'center'}}>
                         <Text style={{ fontSize: 16, color: '#fff', textAlign: 'center'}}>Call To Order</Text>
                        </TouchableOpacity>
                    </View> 
                </View> 
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7',
        height: '100%',
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 5,
    },
    backBtnView: {
        width: '20%',
        padding: 10,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    professionalTitleView: {
        width: '80%',
        paddingLeft: 60,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
});