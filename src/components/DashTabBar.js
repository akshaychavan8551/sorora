import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { createAppContainer, withNavigation } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import Search from './Search'
import MyBookings from './MyBookings'
import Profile from './Profile'
import Login from './Login'
import AllServices from './AllServices'
import Professionals from './Professionals'
import ProfessionalDetails from './ProfessionalDetails'
import OTPVerification from './OTPVerification'
import CreateProfile from './CreateProfile'
import CutomerAddress from './CustomerAddress'
import Addresses from './Addresses'
import EditAddress from './EditAddress'
import ServiceList from './ServiceList'
import BrandList from './BrandList'
import TypeList from './TypeList'
import SelectDateTime from './SelectDateTime'
import PriceSummary from './PriceSummary'
import CallToOrder from './CallToOrder'
import TermsAndConditions from './TermsAndCondtions'
import RecentlySearch from './RecentlySearch'
import Notifications from './Notifications'

const AppTabNavigator = createMaterialTopTabNavigator({
  Search: {
    screen: Search,
    navigationOptions: {
      tabBarLabel: 'Search',
      tabBarIcon: ({ tintColor }) => (
        <Icon name='search' style={[{ color: tintColor }]} size={26} />
      )
    }
  },
  MyBookings: {
    screen: MyBookings,
    navigationOptions: {
      tabBarLabel: 'My Bookings',
      tabBarIcon: ({ tintColor }) => (
        <Icon name='pencil-square-o' style={[{ color: tintColor }]} size={26} />
      )
    }
  },
  Profile: {
    screen: Profile,
    navigationOptions: {
      tabBarLabel: 'Profile',
      tabBarIcon: ({ tintColor }) =>
        <Icon name='user-circle-o' style={[{ color: tintColor }]} size={20} />
    }
  },
}, {
  tabBarPosition: 'bottom',
  tabBarOptions: {
    tabStyle: {
      // borderColor: '#E2E2E2',
      // borderWidth: 0.7, 
    },
    activeTintColor: '#007a68',
    inactiveTintColor: '#707070',
    style: {
      height: 70,
      color: '#707070',
      backgroundColor: '#fff',
    },
    indicatorStyle: {
      height: 68,
      backgroundColor: '#fff',
      activeTintColor: '#007a68',
      borderBottomColor: '#92d344',
      borderBottomWidth: 3,
    },
    upperCaseLabel: false,
    showIcon: true,
    shifting: true,
    labelStyle: {
      fontWeight: 'bold',
      fontSize: 14,
    }
  },
});
const stackNav = createStackNavigator({
  Search: {
    screen: AppTabNavigator,
    navigationOptions: {
      header: null,
    }
  },
  Login: {
    screen: Login,
    navigationOptions: {
      header: null,
    }
  },
  AllServices: {
    screen: AllServices,
    navigationOptions: {
      header: null,
    }
  },
  Professionals: {
    screen: Professionals,
    navigationOptions: {
      header: null,
    }
  },
  ProfessionalDetails: {
    screen: ProfessionalDetails,
    navigationOptions: {
      header: null,
    }
  },
  OTPVerification: {
    screen: OTPVerification,
    navigationOptions: {
      header: null,
    }
  },
  CreateProfile: {
    screen: CreateProfile,
    navigationOptions: {
      header: null,
    }
  },
  CutomerAddress: {
    screen: CutomerAddress,
    navigationOptions: {
      header: null,
    }
  },
  Addresses: {
    screen: Addresses,
    navigationOptions: {
      header: null,
    }
  },
  EditAddress: {
    screen: EditAddress,
    navigationOptions: {
      header: null,
    }
  },
  ServiceList: {
    screen: ServiceList,
    navigationOptions: {
      header: null,
    }
  },
  BrandList: {
    screen: BrandList,
    navigationOptions: {
      header: null,
    }
  },
  TypeList: {
    screen: TypeList,
    navigationOptions: {
      header: null,
    }
  },
  SelectDateTime: {
    screen: SelectDateTime,
    navigationOptions: {
      header: null,
    }
  },
  PriceSummary: {
    screen: PriceSummary,
    navigationOptions: {
      header: null,
    }
  },
  CallToOrder: {
    screen: CallToOrder,
    navigationOptions: {
      header: null,
    }
  },
  TermsAndConditions: {
    screen: TermsAndConditions,
    navigationOptions: {
      header: null,
    }
  },
  RecentlySearch: {
    screen: RecentlySearch,
    navigationOptions: {
      header: null,
    }
  },
  Notifications: {
    screen: Notifications,
    navigationOptions: {
      header: null,
    }
  },
  
});
class DashTabBar extends Component {
  render() {
    return (
      <AppContainer />
    );
  }
}


const AppContainer = createAppContainer(stackNav);


export default withNavigation(DashTabBar);
