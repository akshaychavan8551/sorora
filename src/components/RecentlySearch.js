import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, Alert, Modal, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'UserDatabase.db' }); 

export default class RecentlySearch extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
            spinner: false,
            searchListItems: []
        }
        db.transaction(tx => {
            tx.executeSql('SELECT DISTINCT subcat_id, subcat_name FROM recently_search  ORDER BY search_id DESC LIMIT 10',[], (tx, results) => {
              var temp = [];
              for (let i = 0; i < results.rows.length; ++i) {
                temp.push(results.rows.item(i));
              }
              this.setState({
                searchListItems: temp,
              });
            });
          });
    }
    async componentDidMount() {
        
    }
    goProfessionals = (subcategoryId, subcategoryName) =>{
        this.props.navigation.navigate('Professionals', { subcategory_id: subcategoryId, subcategory_name: subcategoryName })
    }
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                <View style={styles.headerView}>
                    <View style={styles.backBtnView}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image
                                source={require('../assets/images/left_arrow.png')}
                                style={{ height: 25, width: 25 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.professionalTitleView}>
                        <Text style={styles.headerText}>Recently Search</Text>
                    </View>
                </View>
                <View style={{ width: '100%'}}>
                    {this.state.searchListItems.map((data, index)=>{
                        return(
                            <View>
                                <TouchableOpacity onPress={()=> this.goProfessionals(data.subcat_id,data.subcat_name)}>
                                <View style={{ height: 40, borderColor: '#ccc', borderRadius: 10, borderWidth: 1, margin: 10, padding: 10}}>
                                    <View style={{alignContent: 'center', justifyContent: 'center'}}>
                                        <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold'}}>{data.subcat_name}</Text>
                                    </View>     
                                </View> 
                                </TouchableOpacity>
                            </View>
                        ) 
                    })
                    }  
                </View> 
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    {(this.state.searchListItems.length === 0)?
                     <Text style={{ fontSize: 16, fontWeight: 'bold'}}>No recently serach data..</Text>
                     :
                     null
                    }
                </View>       
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7',
        height: '100%',
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        // marginLeft: 5,
    },
    backBtnView: {
        width: '20%',
        padding: 10,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    professionalTitleView: {
        width: '80%',
        paddingLeft: 60,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
});