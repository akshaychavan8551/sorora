import React, { Component } from 'react';
import { StyleSheet, StatusBar, View, TextInput,Keyboard , Text, Image, ScrollView, FlatList, TouchableOpacity, Dimensions, Alert, Modal, ToastAndroid, BackHandler } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';
import NetInfo from "@react-native-community/netinfo";
import Geolocation from '@react-native-community/geolocation';
import AsyncStorage from '@react-native-community/async-storage';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'UserDatabase.db' });

const { height, width } = Dimensions.get('window');

const googleApiKey = 'AIzaSyCRg0gXOIWJpvSCb43GzvCxJKwJU3Tg81o';

export default class Search extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);
        db.transaction(function(txn) {
            txn.executeSql(
              "SELECT name FROM sqlite_master WHERE type='table' AND name='recently_search'",
              [],
              function(tx, res) {
                console.log('item:', res.rows.length);
                if (res.rows.length == 0) {
                  txn.executeSql('DROP TABLE IF EXISTS recently_search', []);
                  txn.executeSql(
                    'CREATE TABLE IF NOT EXISTS recently_search(search_id INTEGER PRIMARY KEY AUTOINCREMENT,subcat_id INTEGER, subcat_name VARCHAR(20))',
                    []
                  );
                }
              }
            );
          });
        this.didBlurListener = this.props.navigation.addListener(
            'didFocus',(obj) => {
              BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
            }
          );
          this.didBlurListener = this.props.navigation.addListener(
            'didBlur',(obj) => {
              BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
             
            }
          );
        this.state = {
            searchService: '',
            categoryData: [],
            subCategoryData: [],
            spinner: false,
            email: '',

            initialPosition: 'unknown',
            lastPosition: 'unknown',
            lat: '',
            long: '',
            modalVisible: false,
            searchSrviceData: [],
            locations: null,

            recentIDArray : [],
            recentCatNameArray : []
        }
    }
    handleBackButton = () => {
        Alert.alert(
            'Exit App',
            'Do you want to exit the app?', [{
                text: 'No',
                // onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'Yes',
                onPress: () => BackHandler.exitApp()
            }, ], {
                cancelable: false
            }
         )
         return true;
    }
    componentDidMount() {
        NetInfo.fetch().then(state => {
            if (state.isConnected == true) {
                this.getCategoryData();
            } else {
                Alert.alert('Network request failed')
            }
        });

       
        Geolocation.getCurrentPosition(
            (position) => {
              this.getLocationDetails(position.coords.latitude,position.coords.longitude)
            },
            (error) => { console.log(error); },
            { enableHighAccuracy: false, enableLowAccuracy:true, timeout: 30000 }
          )
        //remove storage data 
        this.RemoveAllData();
        }
    
        getLocationDetails(latitude, longitude) {
          let location = [];
          fetch('https://maps.googleapis.com/maps/api/geocode/json?address='+ latitude + ',' + longitude + '&key=' + googleApiKey)
          .then((response) => response.json())
          .then((responseJson) => {
              location = responseJson;
              this.setState({ locations:  location.results[5].formatted_address})
          });
        }
        RemoveAllData = async() => {
            await AsyncStorage.removeItem('category_name')
            await AsyncStorage.removeItem('serviceName')
    
            await AsyncStorage.removeItem('provider_id');
            await AsyncStorage.removeItem('subcategory_id');
            await AsyncStorage.removeItem('category_id');
            await AsyncStorage.removeItem('address');
            await AsyncStorage.removeItem('landmark');
            await AsyncStorage.removeItem('service_name');
            await AsyncStorage.removeItem('service_price');
            await AsyncStorage.removeItem('brand_name');
            await AsyncStorage.removeItem('typename');
            await AsyncStorage.removeItem('s_id');
            await AsyncStorage.removeItem('date');
            await AsyncStorage.removeItem('time');
        }

    componentWillUnmount() {
        this.watchID != null && Geolocation.clearWatch(this.watchID);
    }

    getCategoryData = () => {
        this.setState({
            spinner: !this.state.spinner
        });
        return fetch('https://www.omsoftware.org/sorora/api/getAllCategories')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({ categoryData: responseJson.CATEGORY_DATA.slice(0, -1), spinner: false });
                this.setState({ subCategoryData: responseJson.SUBCATEGORY_DATA, spinner: false });
                // console.warn(this.state.subCategoryData)
            })
            .catch((error) => {
                console.log(error)
            //     Alert.alert(
            //         '',
            //         'Unable to process this request. We apologize for any inconvenience. Please try again later.',
            //         [
            //             { text: 'OK', onPress: () => this.setState({ spinner: false }) },
            //         ]
            //     )
            // }).done();
            });
    }
    subscribeUser = () => {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        this.setState({
            spinner: !this.state.spinner
        });
        if(this.state.email === ''){
            Alert.alert('Please enter email id.')
            this.setState({ spinner: false });
        }else if(reg.test(this.state.email) == false){
            Alert.alert('Please enter valid email id.')
            this.setState({ spinner: false });
        }else{
            let fd = new FormData();
            fd.append('email', this.state.email);
            fetch('https://www.omsoftware.org/sorora/api/subscribeAddByApp', {
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
                body: fd
            }).then((response) => response.json())
                .then((responseJson) => {
                    this.setState({ spinner: false });
                    Alert.alert(responseJson.RESPONSE)
                }).catch(err => {
                    console.log(err)
                //     Alert.alert(
                //         '',
                //         'Unable to process this request. We apologize for any inconvenience. Please try again later.',
                //         [
                //             { text: 'OK', onPress: () => this.setState({ spinner: false }) },
                //         ]
                //     )
                // }).done();
                });
            }
    }
    SearchService = (text) => {
        if(text == ''){
            this.setState({searchService: ''})
            this.setState({ modalVisible : false})
        }else{
            this.setState({searchService: text})
            this.getSearchServiceData()
            // setTimeout(()=> this.getSearchServiceData(), 1000)
        }
       
        
    }
    getSearchServiceData = () =>{
        let fd = new FormData();
        fd.append('search', this.state.searchService);
        fetch('https://www.omsoftware.org/sorora/api/searchSubcatbyApp', {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            body: fd
        }).then((response) => response.json())
            .then((responseJson) => {
                // console.log('data: '+responseJson.length)
                    if(responseJson.length > 0){
                        this.setState({ searchSrviceData: responseJson})
                        this.setState({ modalVisible : true})
                        // console.log('full')
                    }else{ 
                        Keyboard.dismiss();
                        this.setState({ SearchService: ''})
                        ToastAndroid.show('Service not found..!', ToastAndroid.LONG)
                    }
            }).catch(err => {
                console.log(err)
            })
    }
    goProfessional = async(subcategoryTd, subcategoryName) => {
        this.setState({modalVisible: false})
        // var id = subcategoryTd.toString();
        db.transaction(function(tx) {
            tx.executeSql(
              'INSERT INTO recently_search (subcat_id, subcat_name) VALUES (?,?)',
              [subcategoryTd, subcategoryName],
              (tx, results) => {
                console.log('Results', results.rowsAffected);
                if (results.rowsAffected > 0) {
                  console.log('Registration data saved successfully')
                } else {
                  console.log('Registration Failed');
                }
              }
            );
          });
        this.props.navigation.navigate('Professionals', { subcategory_id: subcategoryTd, subcategory_name: subcategoryName })
    }
    goAllService = async(subcategoryTd, catname) => {
        var id = subcategoryTd.toString();
        await AsyncStorage.setItem('category_ids', id)
        await AsyncStorage.setItem('category_name', catname)
        console.log(subcategoryTd)
        this.props.navigation.navigate('AllServices', { categoryId: subcategoryTd })
    }
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                <View style={styles.headerView}>
                    <View style={{ flexDirection: 'row'}}>
                        <View>
                            <Text style={styles.locationLabel}>YOUR LOCALITY</Text>
                            <Text style={styles.locationText}>{this.state.locations}</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-end', marginTop: 8, marginRight: 10 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Notifications')}>
                                <Icon name='bell' size={20} color='#fff'/>
                            </TouchableOpacity>
                        </View>    
                    </View>
                    <View style={styles.serachBox}>
                        <View style={styles.searchIconView}>
                            <Icon name="search" color="#fff" />
                        </View>
                        <View>
                            <TextInput
                                selectionColor={'red'} //The highlight (and cursor on iOS) color of the text input.
                                placeholder="Search for a service"
                                placeholderTextColor='#fff'
                                onChangeText={text => this.SearchService(text)}
                                value={this.state.searchService}
                                style={{ paddingLeft: 10, borderBottomWidth: 0, color: '#fff', fontSize: 16 }}
                            />
                        </View>
                    </View>
                </View>
                <ScrollView style={{ backgroundColor: '#f7f7f7', height: '60%' }}>
                    <View style={{ margin: 5 }}>
                        <Text style={styles.recommendedText}>Recommended Services</Text>
                    </View>
                    <ScrollView horizontal={true}>
                        <View>
                            <Image
                                source={require('../assets/images/01.png')}
                                style={styles.sliderImg}
                            />
                        </View>
                        <View>
                            <Image
                                source={require('../assets/images/02.png')}
                                style={styles.sliderImg}
                            />
                        </View>
                    </ScrollView>
                    <View style={{ width: '100%' }}>
                        <FlatList style={{ width: '100%', margin: 5 }}
                            data={this.state.categoryData}
                            numColumns={3}
                            keyExtractor={(categoryData, index) => categoryData.id}
                            renderItem={({ item }) =>
                                <View style={styles.serviceCardView}>
                                    <TouchableOpacity onPress={() => this.goAllService(item.category_id,item.category_name )}>
                                        <View style={{ alignContent: 'center' }}>
                                            <Image
                                                source={{ uri: item.url }}
                                                style={{ height: 30, width: 30, alignSelf: 'center' }}
                                            />
                                            <Text style={styles.serviceCategoryName}>{item.category_name}</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            }
                        />
                    </View>
                    {this.state.subCategoryData.map((data, index) => {
                        return (
                            <View>
                                <View style={{ margin: 5 }}>
                                    <Text style={styles.recommendedText}>{data.category_name}</Text>
                                </View>
                                <ScrollView horizontal={true}>
                                    {data.subcategory_details.map((sub) => {
                                        return (
                                            <View style={{ backgroundColor: '#fff', paddingVertical: 5 }}>
                                                <Image
                                                    source={{ uri: sub.url }}
                                                    style={styles.sliderImg}
                                                />
                                            </View>
                                        )
                                    })
                                    }
                                </ScrollView>
                            </View>
                        )
                    })
                    }
                    <View style={{ margin: 10 }}>
                        <Text style={{ color: '#067c6c', fontSize: 16 }}>GET LATEST POSTS DIRECTLY IN YOUR INBOX</Text>
                    </View>
                    <View style={styles.subscribeView}>
                        <TextInput
                            autoCapitalize='none'
                            autoCompleteType='email'
                            onChangeText={(email) => this.setState({ email })}
                            value={this.state.email}
                            placeholder='Enter Your Email ID Here'
                            style={styles.subscribeInputBox}
                        />
                        <View style={{ width: '26%' }}>
                            <TouchableOpacity onPress={() => this.subscribeUser()}>
                                <View style={styles.subscribeBtnView}>
                                    <Text style={{ color: '#fff' }}>SUBSCRIBE</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
                <View>
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => this.setState({modalVisible: false})}
                        onRequestClose={() => {
                            // Alert.alert('Modal has been closed.');
                        }}>
                        <View style={styles.modalParentView}>
                            <View style={{ height: '60%', width: '80%', backgroundColor: '#fff', alignSelf: 'center', position: 'relative', top: '20%', margin: 15 }}>
                                <ScrollView style={{ height: '60%' }}>
                                    {this.state.searchSrviceData.map((data, index) => {
                                        return (
                                            <View>
                                                <TouchableOpacity onPress={() => this.goProfessional(data.subcategory_id, data.subcategory_name)}>
                                                    <View style={{ alignItems: 'center', justifyContent: 'center', borderColor: '#ccc', borderWidth: 0.5, height: 30, margin: 5,  }}>
                                                        <Text style={{ fontSize: 14, fontWeight: 'normal' }}>{data.subcategory_name}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        )
                                    })
                                    }
                                </ScrollView>
                                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center', marginBottom: 10, backgroundColor: '#f8f8f8', height: 40, elevation: 25 }}>
                                    <TouchableOpacity
                                        style={{height: '100%', width: '100%', justifyContent:'center', alignItems: 'center'}}
                                        onPress={() => {
                                            this.setState({ modalVisible: false, SearchService: '' });
                                        }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 15, color: '#000', paddingBottom: 15 }}>Cancel</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    headerView: {
        backgroundColor: '#007a68',
        padding: 10,
    },
    locationLabel: {
        color: '#fff',
        fontSize: 12,
    },
    locationText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold'
    },
    serachBox: {
        backgroundColor: '#259c78',
        marginVertical: 5,
        height: 40,
        width: '100%',
        borderRadius: 20,
        flexDirection: 'row',
    },
    searchIconView: {
        alignSelf: 'center',
        marginLeft: 10
    },
    recommendedText: {
        fontWeight: 'bold',
        fontSize: 16,
    },
    sliderImg: {
        height: width > 480 ? 180 : 150,
        width: width > 480 ? 400 : 290,
        margin: width > 480 ? 3 : 5
    },
    serviceCardView: {
        backgroundColor: '#fff',
        borderRadius: 10,
        margin: 5,
        height: 100,
        padding: 5,
        width: width > 480 ? '31%':'30%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    serviceCategoryName: {
        color: '#000',
        textAlign: 'center'
    },
    subscribeView: {
        margin: 10,
        flexDirection: 'row',
        width: '100%'
    },
    subscribeInputBox: {
        backgroundColor: '#fff',
        height: 40,
        width: '70%',
        padding: 5
    },
    subscribeBtnView: {
        backgroundColor: '#067c6c',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    }
});