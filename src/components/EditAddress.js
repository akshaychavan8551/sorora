import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { CheckBox } from 'react-native-elements'
import AsyncStorage from '@react-native-community/async-storage';

// import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button'

export default class EditAddress extends Component {
    constructor() {
        super();
        this.state = {
            userId: '',
            addressId: '',
            userAddress: '',
            userLandmark: '',
            userLocality: '',
        }
    }
    componentDidMount() {
        this.getAddressData();
    }
    getAddressData = () => {
        let userId = JSON.stringify(this.props.navigation.getParam('user_id', 'NO-ID'))
        let user_Id = userId.replace(/"/g, "");
        let addressId = JSON.stringify(this.props.navigation.getParam('address_id', 'NO-Address_id'))
        let address_Id = addressId.replace(/"/g, "");
        let address = JSON.stringify(this.props.navigation.getParam('address', 'NO-ADDRESS'))
        let user_address = address.replace(/"/g, "");
        let landmark = JSON.stringify(this.props.navigation.getParam('landmark', 'NO-Landmark'))
        let user_landmark = landmark.replace(/"/g, "");
        let locality = JSON.stringify(this.props.navigation.getParam('locality', 'NO-Locality'))
        let user_locality = locality.replace(/"/g, "");
        if (userId != '') {
            this.setState({ userId: user_Id })
        }
        if (addressId != '') {
            this.setState({ addressId: address_Id })
        }
        if (address != '') {
            this.setState({ userAddress: user_address })
        }
        if (landmark != '') {
            this.setState({ userLandmark: user_landmark })
        }
        if (locality != '') {
            this.setState({ userLocality: user_locality })
        }
    }
    updateUserAddress = () => {
        let fd = new FormData();
        fd.append('user_id', this.state.userId);
        fd.append('address_id', this.state.addressId);
        fd.append('address', this.state.userAddress);
        fd.append('landmark', this.state.userLandmark);
        fd.append('locality', this.state.userLocality);

        fetch('https://omsoftware.org/sorora/api/userUpdateAddres', {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: fd
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.RESPONSECODE == 0) {
                    Alert.alert(responseJson.RESPONSE)
                } else if (responseJson.RESPONSECODE == 1) {
                    Alert.alert(responseJson.RESPONSE)
                    this.props.navigation.navigate('Addresses');

                } else {
                    console.log(responseJson)
                }
            })
            .catch((error) => {
                // console.warn(error)
                Alert.alert(
                    '',
                    'Unable to process this request. We apologize for any inconvenience. Please try again later.',
                    [
                        { text: 'OK' },
                    ]
                )
            }).done();
    }
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <View style={styles.headerView}>
                    <View style={styles.backBtnView}>
                        <TouchableOpacity style={{ height: '100%', width: '100%'}} onPress={() => this.props.navigation.goBack()}>
                            <Image
                                source={require('../assets/images/left_arrow.png')}
                                style={{ height: 25, width: 25 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.professionalTitleView}>
                        <Text style={styles.headerText}>Edit Address</Text>
                    </View>
                </View>
                <View style={styles.addressFormView}>
                    <Text style={styles.addressTitleText}>Where do you need the services ?</Text>
                    <View style={{ width: '100%', marginTop: 15 }}>
                        <TextInput
                            placeholder='Flat/Building/Street'
                            style={styles.inputBox}
                            onChangeText={(userAddress) => this.setState({ userAddress })}
                            value={this.state.userAddress}
                        />
                        <TextInput
                            placeholder='Landmark'
                            style={styles.inputBox}
                            onChangeText={(userLandmark) => this.setState({ userLandmark })}
                            value={this.state.userLandmark}
                        />
                        <TextInput
                            placeholder='Locality'
                            style={styles.inputBox}
                            onChangeText={(userLocality) => this.setState({ userLocality })}
                            value={this.state.userLocality}
                        />
                    </View>
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                    <TouchableOpacity onPress={() => this.updateUserAddress()}>
                        <View style={{ backgroundColor: '#89cf34', height: 45, alignItems: 'center', justifyContent: 'center', margin: 15 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#fff' }}>Update Address</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7',
        height: '100%',
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
    },
    backBtnView: {
        width: '20%',
        padding: 10,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    professionalTitleView: {
        width: '80%',
        paddingLeft: 20,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    addressFormView: {
        padding: 15,
        width: '100%'
    },
    addressTitleText: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    inputBox: {
        backgroundColor: '#fff',
        height: 45,
        // width: '100%',
        paddingLeft: 10,
        marginBottom: 10,
        borderRadius: 5,
        borderColor: '#ccc',
        borderWidth: 0.5
    },
});