import React, { Component } from 'react';
import { StyleSheet, StatusBar, View, Text, TouchableOpacity, Alert } from 'react-native';
import { withNavigation } from 'react-navigation';

class BookService extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props){
        super(props);
    }
    render() {
        return (
            <View style={{flex:1, alignItems:'center', justifyContent: 'center'}}>
                {/* <Text>Book Service</Text> */}
                <View>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'center'}}>Whoops, No Project !</Text>
                    <Text style={{textAlign: 'center'}}>You can start a new project by {'\n'} placing a request now</Text>
                    <TouchableOpacity onPress={()=> Alert.alert('Please go to dashboard and book new service.')}>
                        <View style={{ alignSelf: 'center', backgroundColor: '#56D921', alignItems: 'center', justifyContent: 'center', height: 40, width: 120, borderRadius: 5, marginTop: 10}}>
                            <Text style={{ fontWeight: 'bold'}}>Book a Service</Text>
                        </View>  
                    </TouchableOpacity>  
                </View>
            </View>
        )
    }
}
export default withNavigation(BookService);