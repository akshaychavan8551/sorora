import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, ScrollView, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';

export default class Notifications extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
        }
    }
    render() {
        return(
            <View style={styles.mainView}>
            <StatusBar
                backgroundColor="#067c6c"
                barStyle="light-content"
            />
            <Spinner
                visible={this.state.spinner}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.headerView}>
                <View style={styles.backBtnView}>
                    <TouchableOpacity style={{ height: '100%', width: '100%'}} onPress={() => this.props.navigation.goBack()}>
                        <Image
                            source={require('../assets/images/left_arrow.png')}
                            style={{ height: 25, width: 25 }}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.professionalTitleView}>
                    <Text style={styles.headerText}>Notifications</Text>
                </View>
            </View>
            </View>
        )
    }

}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
    },
    backBtnView: {
        width: '20%',
        padding: 10,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    professionalTitleView: {
        width: '80%',
        paddingLeft: 50,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
});