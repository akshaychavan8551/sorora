import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';

export default class PriceSummary extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
            price: [],
            service: [],
            total: '',
            spinner: false,
            service_name: '',
            category_name: '',
            brands_name: '',
            types_name: '',

        }
    }
    async componentDidMount() {
        // const service_name01 = await AsyncStorage.getItem('service_name');
        // let service_name = JSON.stringify(service_name01)
        // const service_price01 = await AsyncStorage.getItem('service_price');
        // let service_price = JSON.stringify(service_price01)
        let service_price = this.props.navigation.getParam('service_price', 'NO-DATA')
        // let service_price = serv_price.replace(/"/g, "");
        this.setState({ price: service_price })

        let service_name = this.props.navigation.getParam('service_name', 'NO-DATA')
        // let service_name = serv_name.replace(/"/g, "");
        this.setState({ service: service_name })

        // let toatamt = JSON.stringify(this.props.navigation.getParam('total', 'NO-ID'))
        // let total = toatamt.replace(/"/g, "");
        // this.setState({ total: total })
        this.calculateTotal();
        const servName = await AsyncStorage.getItem('serviceName');
        this.setState({ service_name: servName})
        const catName = await AsyncStorage.getItem('category_name');
        this.setState({ category_name: catName})
        const brand_name = await AsyncStorage.getItem('brand_name');
        let b_name = brand_name.replace(/[^a-zA-Z^, ]/g, "");
        this.setState({ brands_name: b_name})
        const type_name = await AsyncStorage.getItem('type_name');
        let t_name = type_name.replace(/[^a-zA-Z^, ]/g, "");
        this.setState({ types_name: t_name})

        
    }
    calculateTotal = async() => {
        // const service_price01 = await AsyncStorage.getItem('service_price');
        // let service_price = JSON.stringify(service_price01)
        let service_price = this.props.navigation.getParam('service_price', 'NO-DATA')
        var total = 0;
        for (var i = 0; i < service_price.length; i++) {
            total += parseInt(service_price[i])
            this.setState({ total: total })
        }
    }
    RemoveAllData = async() => {
        await AsyncStorage.removeItem('category_name')
        await AsyncStorage.removeItem('serviceName')

        await AsyncStorage.removeItem('provider_id');
        await AsyncStorage.removeItem('subcategory_id');
        await AsyncStorage.removeItem('category_id');
        await AsyncStorage.removeItem('address');
        await AsyncStorage.removeItem('landmark');
        await AsyncStorage.removeItem('service_name');
        await AsyncStorage.removeItem('service_price');
        await AsyncStorage.removeItem('brand_name');
        await AsyncStorage.removeItem('type_name');
        await AsyncStorage.removeItem('s_id');
        await AsyncStorage.removeItem('date');
        await AsyncStorage.removeItem('time');
    }
    payOnDelivery = async() => {
        this.setState({
            spinner: !this.state.spinner
        });
        const provider_id = await AsyncStorage.getItem('provider_id');
        const subcategory_id = await AsyncStorage.getItem('subcategory_id');
        const category_id = await AsyncStorage.getItem('category_id');
        const user_id = await AsyncStorage.getItem('userID');
        const user_address = await AsyncStorage.getItem('address');
        const user_landmark = await AsyncStorage.getItem('landmark');
        
        const service_name = await AsyncStorage.getItem('service_name');
        let s_name = service_name.replace(/[^a-zA-Z^, ]/g, "");

        const service_price = await AsyncStorage.getItem('service_price');
        let s_price = service_price.replace(/[^a-zA-Z0-9^,]/g, "");
       
        const brand_name = await AsyncStorage.getItem('brand_name');
        if(brand_name === null){
            var b_name= ''
        }else{
            var b_name = brand_name.replace(/[^a-zA-Z^, ]/g, "");
        }
       
        const type_name = await AsyncStorage.getItem('type_name');
        if(brand_name === null){
            var t_name= ''
        }else{
            var t_name = type_name.replace(/[^a-zA-Z^, ]/g, "");
        }
       

        const s_id = await AsyncStorage.getItem('s_id');
        const date = await AsyncStorage.getItem('date');
        const time = await AsyncStorage.getItem('time');

        console.log(provider_id)
        console.log(subcategory_id)
        console.log(category_id)
        console.log(user_id)
        console.log(user_address)
        console.log(user_landmark)
       
        console.log(s_name);
        console.log(s_price);
        console.log(b_name)
        console.log(t_name)
        console.log(s_id)
        console.log(date)
        console.log(time)


        

        console.log('provider id : ' + provider_id + ' subcategory_id: ' + subcategory_id + ' category_id ' + category_id + ' user_id ' + user_id + ' user_address' + user_address + ' user_landmark' + user_landmark + ' service_name' + service_name + ' service_price' + service_price + ' brand_name' + brand_name + 'type_name' + type_name + 's_id' + s_id + 'date' + date + 'time' + time+ ' total: '+this.state.total)

        let fd = new FormData();
        fd.append('user_id', user_id);
        fd.append('category_id', category_id);
        fd.append('subcategory_id', subcategory_id);
        fd.append('serviceId', s_id);
        fd.append('service_provider_id', provider_id);
        fd.append('bookingServiceLocation', user_address);
        fd.append('landmark', user_landmark);
        fd.append('availableDate', date);
        fd.append('availableTime', time);
        fd.append('price', this.state.total);
        fd.append('serviceName', s_name);
        fd.append('typeName', t_name);
        fd.append('brandName', b_name);
        fd.append('booking_service_price', s_price);


        fetch('https://omsoftware.org/sorora/api/userBookingRequest', {
            method: 'POST',
            headers: { 
                // "Accept-Encoding": "gzip, deflate",
                // 'Accept': 'application/json',
                // 'Content-Type': 'multipart/x-www-form-urlencoded'
                'Content-Type': 'multipart/form-data' 
            },
            body: fd
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.RESPONSECODE == 0) {
                    Alert.alert(responseJson.RESPONSE)
                    this.setState({spinner: false})
                } else if (responseJson.RESPONSECODE == 1) {
                    Alert.alert(responseJson.RESPONSE)
                    this.RemoveAllData();
                    this.setState({spinner: false})
                    this.props.navigation.navigate('Search');
                } else {
                    this.setState({spinner: false})
                    console.log('summary : '+responseJson)
                }
            })
            .catch((error) => {
                // console.warn(error)
                this.setState({spinner: false})
                Alert.alert(
                    '',
                    'Unable to process this request. We apologize for any inconvenience. Please try again later.',
                    [
                        { text: 'OK' },
                    ]
                )
            }).done();
    }
    goBack = async() =>{
        await AsyncStorage.removeItem('provider_id');
        await AsyncStorage.removeItem('subcategory_id');
        await AsyncStorage.removeItem('category_id');
        await AsyncStorage.removeItem('address');
        await AsyncStorage.removeItem('landmark');
        await AsyncStorage.removeItem('service_name');
        await AsyncStorage.removeItem('service_price');
        await AsyncStorage.removeItem('brand_name');
        await AsyncStorage.removeItem('typename');
        await AsyncStorage.removeItem('s_id');
        await AsyncStorage.removeItem('date');
        await AsyncStorage.removeItem('time');
        this.props.navigation.navigate('Addresses')
    }
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                <View style={styles.headerView}>
                    <View style={styles.backBtnView}>
                        <TouchableOpacity style={{ height: '100%', width: '100%'}} onPress={() => this.goBack()}>
                            <Image
                                source={require('../assets/images/left_arrow.png')}
                                style={{ height: 25, width: 25 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.professionalTitleView}>
                        <Text style={styles.headerText}>Price Summary</Text>
                    </View>
                </View>
                <View style={{ width: '100%', padding: 15, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={styles.serviceTitleLabel}> {this.state.category_name} > {this.state.service_name}</Text>
                    <Text style={{ fontWeight: 'bold', fontSize: 16, marginBottom: 5}}>Brand Name : <Text style={{fontWeight: '500', fontSize: 14}}>{this.state.brands_name}</Text></Text>
                    <Text style={{ fontWeight: 'bold', fontSize: 16, marginBottom: 20}}>Type Name : <Text style={{fontWeight: '500', fontSize: 14}}>{this.state.types_name}</Text></Text>
                    <View style={{ flexDirection: 'row', width: '100%', marginBottom: 15 }}>
                        <View style={{ width: '20%' }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Sr. NO</Text>
                        </View>
                        <View style={{ width: '60%' }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Service</Text>
                        </View>
                        <View style={{ width: '20%' }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Amount</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row', width: '100%' }}>
                        <View style={{ width: '20%' }}>
                            {this.state.service.map((name, index) => {
                                return (
                                    <Text style={{ fontSize: 16, fontWeight: '400', marginVertical: 5 }}>{index + 1}</Text>
                                )

                            })
                            }
                        </View>
                        <View style={{ width: '60%' }}>
                            {this.state.service.map((name) => {
                                return (
                                    <Text style={{ fontSize: 16, fontWeight: '400', marginVertical: 5 }}>{name}</Text>
                                )

                            })
                                // <Text style={{ fontSize: 16, fontWeight: '400' }}>{this.state.service}</Text>
                            }
                        </View>
                        <View style={{ width: '20%' }}>
                            {this.state.price.map((price) => {
                                return (
                                    <Text style={{ fontSize: 16, fontWeight: '400', marginVertical: 5 }}>{price}</Text>
                                )

                            })
                                //   <Text style={{ fontSize: 16, fontWeight: '400' }}>{this.state.price}</Text>
                            }

                        </View>
                    </View>
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end', marginRight: 15 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 18, alignSelf: 'flex-end' }}>Total :
                            {this.state.total}
                    </Text>
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', alignItems: 'flex-end', marginBottom: 5, width: '100%' }}>
                    <View>
                        <TouchableOpacity onPress={() => this.payOnDelivery()}>
                            <View style={styles.deliveryBtnView}>
                                <Text style={styles.nextBtnText}>PAY ON DELIVERY</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity>
                            <View style={styles.payBtnView}>
                                <Text style={styles.nextBtnText}>PAY NOW</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7',
        height: '100%',
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
    },
    backBtnView: {
        width: '20%',
        padding: 10,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    professionalTitleView: {
        width: '80%',
        paddingLeft: 60,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    serviceTitleLabel: {
        fontSize: 16,
        fontWeight: 'bold',
        marginVertical: 20,
        textAlign: 'center',
    },
    deliveryBtnView: {
        backgroundColor: '#89cf34',
        height: 50,
        width: 170,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 15
    },
    payBtnView: {
        backgroundColor: '#89cf34',
        height: 50,
        width: 150,
        alignItems: 'center',
        justifyContent: 'center',
    },
    nextBtnText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16
    }
})
