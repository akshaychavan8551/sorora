import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, Alert, Modal,ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import DatePicker from 'react-native-datepicker'

export default class SelectDateTime extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
            spinner: false,
            date: '',
            time: '',
            timeData: [],
            modalVisible: false,
            currentdate: new Date().getDate()+6,
            maxdate:''
            // maxdate: new Date().getFullYear()+'-'+ new Date().getMonth()+1+'-'+currentdate
        }
    }
    componentDidMount() {
        this.getTimeData()
       this.getCurrentDate()
    }
    getCurrentDate = () => {
        let date = new Date().getFullYear()+'-'+this.state.currentdate+'-'+ new Date().getMonth()+1
        this.setState({maxdate: date})
        console.log( new Date().getFullYear()+'-'+this.state.currentdate+'-'+ new Date().getMonth()+1)
    }
    getTimeData = async() => {
        this.setState({
            spinner: !this.state.spinner
        });
      
        const provider_ids = await AsyncStorage.getItem('provider_id');
        const subcategory_ids = await AsyncStorage.getItem('subcategory_id');
        const category_ids = await AsyncStorage.getItem('category_id');
    
        console.log('Time List : '+ 'provider_id: '+provider_ids + 'subcategory_id: '+subcategory_ids + 'category_id: '+category_ids)
        var apiHeader =  {
            method: 'POST',
            headers: {
                // Accept: 'application/json',
                "Content-Type": "application/json",
                "Connection": "keep-alive",
                "Content-Type": "application/x-www-form-urlencoded",
                "accept-encoding": "gzip,deflate",
                "cache-control": "no-cache,no-cache"
            },
            body: "subcategory_id="+subcategory_ids+"&sprovider_id="+provider_ids+"&category_id="+category_ids
        }
        fetch('https://omsoftware.org/sorora/api/getTimeBySpidandSubcatidandCatid',apiHeader
        ).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.RESPONSECODE == 0) {
                    this.setState({spinner: false})
                    alert(responseJson.RESPONSE)
                } else if (responseJson.RESPONSECODE == 1) {
                    this.setState({ timeData: responseJson.TIME, spinner: false })
                    console.log(responseJson.TIME)

                } else {
                    this.setState({ message: responseJson, spinner: false})
                    console.log(responseJson)
                }
            })
            .catch((error) => {
                this.setState({spinner: false})
                 console.log(error)
                Alert.alert(
                    '',
                    'Server error. Please try again later.',
                    [
                        { text: 'OK' },
                    ]
                )
            }).done();
            // })
    }
    goToNextProcess = async() => {
       

        let service_name = this.props.navigation.getParam('service_name', 'NO-ID')
       
        let service_price = this.props.navigation.getParam('service_price', 'NO-ID')

        if (this.state.date != '' && this.state.time != '') {
            await AsyncStorage.setItem('date', this.state.date)
            await AsyncStorage.setItem('time', this.state.time)
            this.props.navigation.navigate('PriceSummary', {service_name: service_name, service_price: service_price})
            
        } else {
            Alert.alert('Please select date and time')
        }
    }
    goBack = async() =>{
        await AsyncStorage.removeItem('provider_id');
        await AsyncStorage.removeItem('subcategory_id');
        await AsyncStorage.removeItem('category_id');
        await AsyncStorage.removeItem('s_id')
        await AsyncStorage.removeItem('address')
        await AsyncStorage.removeItem('landmark')
        await AsyncStorage.removeItem('service_name');
        await AsyncStorage.removeItem('service_price');
        await AsyncStorage.removeItem('brand_name');
       await AsyncStorage.removeItem('typename');
        await AsyncStorage.removeItem('date')
        await AsyncStorage.removeItem('time')
        this.props.navigation.navigate('Addresses')
    }
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <View style={styles.headerView}>
                    <View style={styles.backBtnView}>
                        <TouchableOpacity style={{ height: '100%', width: '100%'}} onPress={() => this.goBack()}>
                            <Image
                                source={require('../assets/images/left_arrow.png')}
                                style={{ height: 25, width: 25 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.professionalTitleView}>
                        <Text style={styles.headerText}>Select Date Time</Text>
                    </View>
                </View>
                <Text style={styles.serviceTitleLabel}>Select date of service</Text>
                <View style={{ width: '100%', padding: 15 }}>
                    <DatePicker
                        style={{ width: '100%' }}
                        date={this.state.date}
                        mode="date"
                        placeholder="select date"
                        format="dddd YYYY MMM, DD"
                        minDate={new Date()}
                        // maxDate="2100-07-01"
                        // maxDate={this.state.maxdate}
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                            dateIcon: {
                                position: 'absolute',
                                left: 0,
                                top: 4,
                                marginLeft: 0
                            },
                            dateInput: {
                                marginLeft: 36
                            }
                            // ... You can check the source to find the other keys.
                        }}
                        onDateChange={(date) => { this.setState({ date: date }) }}
                    />
                </View>
                <View>
                    <Text style={styles.serviceTitleLabel}>select time</Text>
                    <View style={{ width: '100%', height: 40}}>
                        <TouchableOpacity onPress={()=> this.setState({ modalVisible: true})}>
                            <View style={{  borderColor: '#ccc', borderWidth: 0.7, height: 40, margin: 15, width: '93%' }}>
                                {(this.state.time =='')?
                                    <View style={{ flexDirection: 'row'}}>
                                        <View style={{height: 40, width: '90%', alignItems: 'center', justifyContent: 'center'}}>
                                            <Text style={{ color: '#000', marginLeft: 20}}>please select time</Text>
                                        </View>
                                        <View style={{ height: 40, width: '10%', alignItems: 'center', justifyContent:'center'}}>
                                            <Icon name='caret-down' size={20} color='#ccc'/>
                                        </View>
                                    </View>
                                :
                                    <View style={{ flexDirection: 'row'}}>
                                        <View style={{height: 40, width: '90%', alignItems: 'center', justifyContent: 'center'}}>
                                            <Text style={{ color: '#000', marginLeft: 20}}>{this.state.time}</Text>
                                        </View>
                                        <View style={{ height: 40, width: '10%', alignItems: 'center', justifyContent:'center'}}>
                                            <Icon name='caret-down' size={20} color='#ccc'/>
                                        </View>
                                    </View>
                                }
                            </View>    
                        </TouchableOpacity>
                    </View>
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => this.setState({modalVisible: false})}
                        onRequestClose={() => {
                            // Alert.alert('Modal has been closed.');
                        }}>
                        <View>
                            <View style={{ height: '60%', width: '80%', backgroundColor: '#fff', alignSelf: 'center', position: 'relative', top: '60%', margin: 15 }}>
                                <ScrollView style={{ height: '60%' }}>
                                    {this.state.timeData.map((data, index) => {
                                        return (
                                            <View>
                                                <TouchableOpacity onPress={() => this.setState({time: data, modalVisible: false})}>
                                                    <View style={{ alignItems: 'center', justifyContent: 'center', borderColor: '#ccc', borderWidth: 0.5, height: 30, margin: 5,  }}>
                                                        <Text style={{ fontSize: 14, fontWeight: 'normal' }}>{data}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        )
                                    })
                                    }
                                </ScrollView>
                                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center', marginBottom: 10, backgroundColor: '#f8f8f8', height: 40, elevation: 25 }}>
                                    <TouchableOpacity
                                        style={{height: '100%', width: '100%', justifyContent:'center', alignItems: 'center'}}
                                        onPress={() => {
                                            this.setState({ modalVisible: false });
                                        }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 15, color: '#000', paddingBottom: 15 }}>Cancel</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                    {/* <View style={{ width: '100%', padding: 15 }}>
                        <DatePicker
                            style={{ width: '100%' }}
                            date={this.state.time}
                            // is24Hour={true}
                            mode="time"
                            placeholder="select time"
                            format="hh:mm a"
                            excludeOutOfBoundTimes
                            minDate={new Date()}
                            minTime={new Date()}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 36
                                }
                            }}
                            onDateChange={(time) => { this.setState({ time: time }) }}
                        />
                    </View> */}
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center', marginBottom: 5 }}>
                    <TouchableOpacity onPress={() => this.goToNextProcess()}>
                        <View style={styles.nextBtnView}>
                            <Text style={styles.nextBtnText}>NEXT</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7',
        height: '100%',
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        // marginLeft: 5,
    },
    backBtnView: {
        width: '20%',
        padding: 10,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    professionalTitleView: {
        width: '80%',
        paddingLeft: 60,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    serviceTitleLabel: {
        fontSize: 16,
        fontWeight: 'bold',
        marginVertical: 20,
        textAlign: 'center',
    },
    nextBtnView: {
        backgroundColor: '#89cf34',
        height: 50,
        width: 150,
        alignItems: 'center',
        justifyContent: 'center'
    },
    nextBtnText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16
    }
})
