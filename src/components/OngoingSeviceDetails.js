import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, FlatList, Modal, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';


export default class OngoingServiceDetails extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
            spinner: false,
            bookingData: [],
            bookingImages: [],
            service_name: [],
            url: ''
            
        }
        
    }
    componentDidMount() {
        this.getdata();
    }
    getdata = () => {
        let bookingId = this.props.navigation.getParam('booking_id', '')
        console.log(bookingId)
        // let b_id = bookingId.replace(/"/g, "");
        var apiHeader = {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Connection": "keep-alive",
                "Content-Type": "application/x-www-form-urlencoded",
                "accept-encoding": "gzip,deflate",
                "cache-control": "no-cache,no-cache"
            },
            body: "booking_id="+bookingId
        }
        fetch('https://omsoftware.org/sorora/api/getDetailsBookingsByBookingId', apiHeader
        ).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.RESPONSECODE == 0) {
                    console.log(responseJson.RESPONSE)
                    alert(responseJson.RESPONSE)
                }else if (responseJson.RESPONSECODE == 1) {
                    this.setState({ bookingData: responseJson.DATA})
                    this.setState({ bookingImages: responseJson.DATA.allImages})
                    this.setState({ service_name: responseJson.DATA.booking_service_name}),
                    console.log(responseJson.DATA.allImages)
                }else {
                    // console.log(responseJson)
                }
            })
            .catch((error) => {
                console.log(error)
            })
    }
    
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                {/* <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                /> */}
                {/* <View style={styles.headerView}>
                    <View style={styles.backBtnView}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image
                                source={require('../assets/images/left_arrow.png')}
                                style={{ height: 25, width: 25 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.professionalTitleView}>
                        <Text style={styles.headerText}>Service Details</Text>
                    </View>
                </View> */}
                {/* <View>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <View style={{flexDirection: 'row', alignItems: 'center', margin: 5, borderBottomColor: '#ccc', borderBottomWidth: 1, width: '20%', paddingBottom: 5}}>
                            <Icon name='long-arrow-left' size={20} color='#000'/>
                            <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#000'}}>&nbsp;&nbsp;Back</Text>
                        </View>
                    </TouchableOpacity>
                </View>     */}
                <View style={{ margin: 5, padding: 5, borderColor: '#ccc', borderWidth: 1}}>
                    <View style={{ width: '100%', backgroundColor: '#ccc',height: 30, justifyContent: 'center', padding: 5}}>
                        <Text style={{ fontSize: 16, fontWeight: 'bold'}}>Service Details</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginVertical: 10}}>
                        <View style={{ width: '35%'}}>
                            <Text style={{ fontSize: 14, fontWeight: 'bold'}}>Service Provider Name : </Text>
                        </View>
                        <View style={{ width: '65%'}}>
                            <Text style={{ fontSize: 14, fontWeight: '500'}}>{this.state.bookingData.sp_name}</Text>
                        </View>
                    </View>  
                    <View style={{ flexDirection: 'row', marginVertical: 10}}>
                        <View style={{ width: '35%'}}>
                            <Text style={{ fontSize: 14, fontWeight: 'bold'}}>Service Name : </Text>
                        </View>
                        <View style={{ width: '65%'}}>
                            <Text style={{ fontSize: 14, fontWeight: '500'}}>{this.state.bookingData.subcategory_name}</Text>
                        </View>
                    </View> 
                    <View style={{ flexDirection: 'row', marginVertical: 10}}>
                        <View style={{ width: '35%'}}>
                            <Text style={{ fontSize: 14, fontWeight: 'bold'}}>Description : </Text>
                        </View>
                        <View style={{ width: '65%'}}>
                            <Text style={{ fontSize: 14, fontWeight: '500'}}>{this.state.bookingData.s_description}</Text>
                        </View>
                    </View> 
                    <View style={{ flexDirection: 'row', marginVertical: 10}}>
                        <View style={{ width: '35%'}}>
                            <Text style={{ fontSize: 14, fontWeight: 'bold'}}>Date {'&'} Time : </Text>
                        </View>
                        <View style={{ width: '65%'}}>
                            <Text style={{ fontSize: 14, fontWeight: '500'}}>{this.state.bookingData.available_date},&nbsp; {this.state.bookingData.available_time}</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row',marginVertical: 10}}>
                        <View style={{ width: '35%'}}>
                            <Text style={{ fontSize: 14, fontWeight: 'bold'}}>Location : </Text>
                        </View>
                        <View style={{ width: '65%'}}>
                            <Text style={{ fontSize: 14, fontWeight: '500'}}>{this.state.bookingData.booking_service_location},&nbsp; {this.state.bookingData.service_landmark}</Text>
                        </View>
                    </View>   
                    <View style={{ flexDirection: 'row', marginVertical: 10}}>
                        <View style={{ width: '35%'}}>
                            <Text style={{ fontSize: 14, fontWeight: 'bold'}}>Price : </Text>
                        </View>
                        <View style={{ width: '65%'}}>
                            <Text style={{ fontSize: 14, fontWeight: '500'}}>{this.state.bookingData.booking_service_name}&nbsp;&nbsp;{this.state.bookingData.booking_service_price}&nbsp;&nbsp;</Text>
                        </View>
                    </View> 
                    <View style={{ flexDirection: 'row', marginVertical: 10}}>
                        <View style={{ width: '35%'}}>
                            <Text style={{ fontSize: 14, fontWeight: 'bold'}}></Text>
                        </View>
                        <View style={{ width: '65%'}}>
                            <Text style={{ fontSize: 14, fontWeight: '500'}}> <Text style={{ fontWeight: 'bold', fontSize: 16}}>Total:</Text> &nbsp;{this.state.bookingData.s_currency}&nbsp;{this.state.bookingData.price}</Text>
                        </View>
                    </View> 
                 </View>
                 <View style={{ margin: 5, padding: 5, borderColor: '#ccc', borderWidth: 1}}>
                    <View style={{ width: '100%', backgroundColor: '#ccc',height: 30, justifyContent: 'center', padding: 5}}>
                        <Text style={{ fontSize: 16, fontWeight: 'bold'}}>Gallery</Text>
                    </View>
                    <View>
                        {(this.state.bookingImages.length == 0)?
                            <Text>Images not available</Text>
                            :
                            <FlatList style={{ width: '100%', margin: 5 }}
                                data={this.state.bookingImages}
                                numColumns={6}
                                keyExtractor={(bookingImages, index) => bookingImages.imgName}
                                renderItem={({ item }) =>
                                    <View>
                                        <View style={{ alignContent: 'center' }}>
                                            <Image
                                                source={{ uri: item.imgName }}
                                                style={{ height: 50, width: 50, alignSelf: 'center', backgroundColor: '#ccc', margin: 5 }}
                                            />
                                        </View>
                                    </View>
                                }
                            />
                               
                                // <Image
                                //     source={{ uri: this.state.bookingData.allImages.imgName }}
                                //     style={{ height: 25, width: 25 }}
                                // /> 
                        }
                    </View>    
                 </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7',
        height: '100%',
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        // marginLeft: 5,
    },
    backBtnView: {
        width: '20%',
        padding: 10,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    professionalTitleView: {
        width: '80%',
        paddingLeft: 60,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
});