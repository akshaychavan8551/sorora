import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import { View } from 'react-native'
import DashTabBar from './DashTabBar';

class Dashboard extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
        }
    }

    render() {
        return (
            <View style={{ height: '100%' }}>
                <DashTabBar />
            </View>
        )
    }
}
export default withNavigation(Dashboard);
