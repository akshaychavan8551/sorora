import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import AsyncStorage from '@react-native-community/async-storage';

export default class OTPVerification extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
            mobile: '',
            dialCode: '',
            countryClass: '',
            countryTitle: '',
            otp: '',

            setName: '',
            setContact: '',
            setEmail: '',
            setUserID: '',
            setUserAddress: '',
            setCountryClass: '',
            setUserProfile: '',
        }
    }
    componentDidMount() {
        this.getData();
    }
    getData = () => {
        let mobile = JSON.stringify(this.props.navigation.getParam('mobile', ''))
        let mobilenum = mobile.replace(/"/g, "");
        this.setState({ mobile: mobilenum })

        let dailCodenum= JSON.stringify(this.props.navigation.getParam('dialCode', ''))
        let dailCode01 = dailCodenum.replace(/"/g, "");
        this.setState({ dialCode: dailCode01 })

        let countryClass00= JSON.stringify(this.props.navigation.getParam('countryClass', ''))
        let countryClass01 = countryClass00.replace(/"/g, "");
        this.setState({ countryClass: countryClass01 })

        let countryTitle00= JSON.stringify(this.props.navigation.getParam('countryTitle', ''))
        let countryTitle01 = countryTitle00.replace(/"/g, "");
        this.setState({ countryTitle: countryTitle01 })
    }
    setUserData = async () => {
        await AsyncStorage.setItem('user_name', this.state.setName)
        await AsyncStorage.setItem('user_email', this.state.setEmail)
        await AsyncStorage.setItem('user_mobile', this.state.setContact)
        await AsyncStorage.setItem('userID', this.state.setUserID)
        await AsyncStorage.setItem('userCountryClass', this.state.setCountryClass)
        await AsyncStorage.setItem('user_Profile', this.state.setUserProfile)
        let address = this.state.setUserAddress.replace(/"/g, "");
        await AsyncStorage.setItem('user_Address', address)
    }
    verifyUser = () => {
        if(this.state.otp == ''){
            Alert.alert('Please enter otp.')
        }else{
            let fd = new FormData();
            fd.append('mobile', this.state.mobile);
            fd.append('otp', this.state.otp);
            fetch('https://omsoftware.org/sorora/api/verifyOtp', {
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                body: fd
            }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.RESPONSECODE == 0) {
                        Alert.alert(responseJson.RESPONSE)
                    }
                    else if (responseJson.RESPONSECODE == 1) {
                        Alert.alert(responseJson.RESPONSE);
                        this.setState({ setName: responseJson.user_name })
                        this.setState({ setEmail: responseJson.user_email })
                        this.setState({ setContact: responseJson.user_contact })
                        this.setState({ setCountryClass: responseJson.country_title })
                        this.setState({ setUserProfile: responseJson.image })
                        let id = JSON.stringify(responseJson.user_id)
                        this.setState({ setUserID: id })
                        let address = JSON.stringify(responseJson.user_service_address)
                        this.setState({ setUserAddress: address })
                        console.log('setname: ', this.state.setName);
                        this.setUserData();
                        this.props.navigation.navigate('Search')
                        // this.props.navigation.navigate('Addresses',{user_id: id})
                    }
                    else if (responseJson.RESPONSE == "New User!") {
                        this.props.navigation.navigate('CreateProfile', { mobile: this.state.mobile, dialCode: this.state.dialCode, countryClass: this.state.countryClass, countryTitle: this.state.countryTitle })
                    }
                })
                .catch((error) => {
                    // console.warn(error)
                    Alert.alert(
                        '',
                        'Unable to process this request. We apologize for any inconvenience. Please try again later.',
                        [
                            { text: 'OK' },
                        ]
                    )
                }).done();
            }
    }
    ResendOTPVerification = () => {
        let fd = new FormData();
        fd.append('mobile', this.state.mobile);
        fetch('https://omsoftware.org/sorora/api/sendOtp', {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: fd
        }).then((response) => response.json())
            .then((responseJson) => {
                Alert.alert('New OTP :  ' + responseJson.OTP);
            })
            .catch((error) => {
                // console.warn(error)
                Alert.alert(
                    '',
                    'Unable to process this request. We apologize for any inconvenience. Please try again later.',
                    [
                        { text: 'OK' },
                    ]
                )
            }).done();
    }
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <View style={styles.headerView}>
                    <View style={styles.backBtnView}>
                        <TouchableOpacity style={{ height: '100%', width: '100%'}} onPress={() => this.props.navigation.goBack()}>
                            <Image
                                source={require('../assets/images/left_arrow.png')}
                                style={{ height: 25, width: 25 }}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ marginTop: 100, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#fff', marginBottom: 10 }}>OTP Verification</Text>
                    <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#fff', marginBottom: 20 }}>Enter OTP sent to : {this.state.dialCode} {this.state.mobile}</Text>
                    <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                        <OTPInputView
                            style={{ width: '80%', height: 100 }}
                            pinCount={4}
                            code={this.state.otp} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                            onCodeChanged={otp => { this.setState({ otp }) }}
                            autoFocusOnLoad
                            codeInputFieldStyle={styles.underlineStyleBase}
                            codeInputHighlightStyle={styles.underlineStyleHighLighted}
                            onCodeFilled={(otp => {
                                console.log(`Code is ${otp}, you are good to go!`)
                            })}
                        />
                    </View>
                    <View style={{ flexDirection: 'row',  marginTop: 20}}>
                        <Text style={{ color: '#fff', fontSize: 16 }}>Don't receive the OTP ? &nbsp;&nbsp;</Text>
                        <View>
                            <TouchableOpacity onPress={()=> this.ResendOTPVerification()}>
                                <Text style={{ fontWeight: 'bold', color: '#fff', fontSize: 16 }}>RESEND OTP</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ marginTop: 25 }}>
                        <TouchableOpacity onPress={() => this.verifyUser()}>
                            <View style={{ backgroundColor: '#89cf34', alignItems: 'center', justifyContent: 'center', width: 270, height: 40, borderRadius: 10 }}>
                                <Text style={{ fontSize: 16, color: '#fff' }}>Verify {'\&'} Continue</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#007a68',
        height: '100%',
        padding: 15,
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        //justifyContent: 'center',
    },
    backBtnView: {
        width: '20%',
        //padding: 10,
        alignItems: 'flex-start',
        //justifyContent: 'center',
    },

    // 
    borderStyleBase: {
        width: 30,
        height: 45,
    },

    borderStyleHighLighted: {
        borderColor: "#03DAC6",
    },

    underlineStyleBase: {
        backgroundColor: '#fff',
        borderRadius: 5,
        width: 50,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
        color: '#000',
        fontSize: 16,
        fontWeight: 'bold'
    },

    underlineStyleHighLighted: {
        borderColor: "#03DAC6",
    },

});