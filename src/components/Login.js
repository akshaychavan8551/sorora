import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, Alert, BackHandler } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-community/google-signin';
import Spinner from 'react-native-loading-spinner-overlay';
import PhoneInput, {isValidPhoneNumber } from 'react-native-phone-input';
import ModalPickerImage from '../../ModalPickerImage';
import AsyncStorage from '@react-native-community/async-storage';
import { LoginButton, LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';

export default class Login extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);
        // this.didBlurListener = this.props.navigation.addListener(
        //     'didFocus',(obj) => {
        //       BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        //     }
        //   );
        //   this.didBlurListener = this.props.navigation.addListener(
        //     'didBlur',(obj) => {
        //       BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
             
        //     }
        //   );
        this.onPressFlag = this.onPressFlag.bind(this);
        this.selectCountry = this.selectCountry.bind(this);

        this.state = {
            pickerData: null,
            mobile: '',
            dialcode: '+254',
            countryClass: '',
            countryTitle:'',
            spinner: false,

            socialUsername: '',
            socialEmail: '',

            // for storage
            setName: '',
            setContact: '',
            setEmail: '',
            setUserID: '',
            setUserAddress: '',
        };
        GoogleSignin.configure({
            scopes: ['https://www.googleapis.com/auth/drive.readonly'],
            webClientId: '986558245654-4lenif65v871l9lq0vcl2pg11r9kp9i2.apps.googleusercontent.com',
            offlineAccess: true,
            hostedDomain: '',
            loginHint: '',
            forceConsentPrompt: true,
            accountName: '',
            iosClientId: '986558245654-4lenif65v871l9lq0vcl2pg11r9kp9i2.apps.googleusercontent.com',
        });
    }
    // handleBackButton = () => {
    //     Alert.alert(
    //         'Exit App',
    //         'Do you want to exit the app?', [{
    //             text: 'No',
    //             // onPress: () => console.log('Cancel Pressed'),
    //             style: 'cancel'
    //         }, {
    //             text: 'Yes',
    //             onPress: () => BackHandler.exitApp()
    //         }, ], {
    //             cancelable: false
    //         }
    //      )
    //      return true;
    // } 
    async componentDidMount() {
        this.setState({
            pickerData: this.phone.getPickerData(),
        });

        var thisObj = this;
        GoogleSignin.configure();
    };
    onPressFlag() {
        this.myCountryPicker.open();
    }

    selectCountry(country) {
        console.log(this.state.pickerData)
        console.log(country.dialCode)
        this.phone.selectCountry(country.dialCode);
        this.setState({dialcode: country.dialCode})
        this.setState({countryClass: country.iso2})
        this.setState({ countryTitle: country.label+': '+country.dialCode})
    }
    goOTPVerification = () => {
        var reg = /^\d+$/;
        if (this.state.mobile == '') {
            Alert.alert('Please Enter Mobile Number')
        } else if(reg.test(this.state.mobile) == false){
            alert('Please enter valid.')
        }else {
            let fd = new FormData();
            fd.append('mobile', this.state.mobile);
            fetch('https://omsoftware.org/sorora/api/sendOtp', {
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                body: fd
            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.navigation.navigate('OTPVerification', { mobile: this.state.mobile, dialCode: this.state.dialcode, countryClass: this.state.countryClass, countryTitle: this.state.countryTitle });
                    // ToastAndroid.showWithGravityAndOffset(
                    //     JSON.stringify(responseJson.OTP),
                    //     ToastAndroid.LONG,
                    //     ToastAndroid.BOTTOM,
                    //     25,
                    //     50,
                    //   );
                    Alert.alert('OTP :  ' + responseJson.OTP);
                })
                .catch((error) => {
                    // console.warn(error)
                    Alert.alert(
                        '',
                        'Unable to process this request. We apologize for any inconvenience. Please try again later.',
                        [
                            { text: 'OK' },
                        ]
                    )
                }).done();
        }
    }
    setUserData = async () => {
        await AsyncStorage.setItem('user_name', this.state.setName)
        await AsyncStorage.setItem('user_email', this.state.setEmail)
        await AsyncStorage.setItem('user_mobile', this.state.setContact)
        await AsyncStorage.setItem('userID', this.state.setUserID)
        await AsyncStorage.setItem('user_Address', this.state.setUserAddress)
        await AsyncStorage.setItem('user_Gender', this.state.value)
    }
    goWithSocialMediaLogin = (name, email) => {
        this.setState({
            spinner: !this.state.spinner
        });
        this.setState({ socialUsername: name, socialEmail: email })
        let fd = new FormData();
        fd.append('name', name);
        fd.append('email_id', email);
        fetch('https://omsoftware.org/sorora/api/registerUserForSocialMedia', {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            body: fd
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.RESPONSECODE == 1) {
                    this.setState({ setName: responseJson.user_name })
                    this.setState({ setEmail: responseJson.user_email })
                    this.setState({ setContact: responseJson.user_contact })
                    let id = JSON.stringify(responseJson.user_id)
                    this.setState({ setUserID: id })
                    let address = JSON.stringify(responseJson.user_service_address)
                    this.setState({ setUserAddress: address })
                    this.setUserData();
                    this.setState({ spinner: false });
                    Alert.alert(responseJson.RESPONSE)
                    this.props.navigation.navigate('Search');
                } else if (responseJson.RESPONSECODE == 2) {
                    this.props.navigation.navigate('CreateProfile', { name: this.state.socialUsername, email: this.state.socialEmail })
                    this.setState({ spinner: false });
                }
            }).catch(err => {
                console.log(err)

            })

    }
    _responseInfoCallback(error: ?Object, result: ?Object) {
        if (error) {
            console.log('Error fetching data: ' + error.toString());
        } else if (result.name == '' || result.email == '') {
            LoginManager.logOut();
            alert('fetching data null. please use another account')
        }
        else {
            this.goWithSocialMediaLogin(result.name, result.email)
            // this.setState({ username: result.name})
            // console.warn(result)
            console.log(result, 'facebook');
        }
    }
    fbAuth() {
        var thisObj = this;
        const infoRequest = new GraphRequest(
            '/me?fields=id,name,email',
            null,
            this._responseInfoCallback.bind(this),
        );

        LoginManager.logInWithPermissions(['public_profile', 'email']).then(
            function (result) {
                if (result.isCancelled) {
                    console.log("Login cancelled");
                } else {
                    thisObj.setState({
                        spinner: true
                    })
                    new GraphRequestManager().addRequest(infoRequest).start();

                    console.log(
                        "Login success with permissions: " +
                        result.grantedPermissions.toString()
                    );
                }
            }.bind(this),
            function (error) {
                console.log("Login fail with error: " + error);
            }
        );
    }
    googleLogin = async () => {
        try {
            // await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
        } catch (error) {
            console.log('google sign out error in login', error);
        }
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            this.goWithSocialMediaLogin(userInfo.user.name, userInfo.user.email)
            console.log(userInfo)
        } catch (error) {
            console.log(error.code)
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // Alert.alert('user cancelled the login flow');
            } else if (error.code === statusCodes.IN_PROGRESS) {
                Alert.alert('Operation (f.e. sign in) is in progress already');
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                Alert.alert('Play services not available or outdated');
            } else {
                Alert.alert('Some other error happened');
            }
        }
    };
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                <View>
                    <Text style={styles.titleText}>Welcome to SORORA!</Text>
                    <View style={styles.inputParentView}>
                        <View style={{ width: '25%' }}>
                            <View style={{ height: 40, borderColor: '#ccc', borderWidth: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <PhoneInput
                                    ref={(ref) => {
                                        this.phone = ref;
                                    }}
                                    initialCountry='ke'
                                    // disabled={true}
                                    value={this.state.dialcode}
                                    onPressFlag={this.onPressFlag}
                                    // isValidPhoneNumber={(JSON.stringify(this.state.dialcode+' '+this.state.mobile)) === true? console.log('done') : console.log(JSON.stringify(this.state.mobile))}
                                    style={{ paddingLeft: 10, color: '#000' }}
                                />
                                <ModalPickerImage
                                    ref={(ref) => {
                                        this.myCountryPicker = ref;
                                        // console.log(ref)
                                    }}
                                    data={this.state.pickerData}
                                    onChange={(country) => {
                                        this.selectCountry(country);
                                        // console.log(country.iso2)
                                        // console.log(this.state.pickerData)
                                    }}
                                    cancelText="Cancel"
                                />
                            </View>
                        </View>
                        <View style={{ width: '75%' }}>
                            <TextInput
                                // maxLength={10}
                                keyboardType='phone-pad'
                                placeholder="Mobile Number"
                                style={styles.mobileInputBox}
                                onChangeText={(mobile) => this.setState({ mobile })}
                                value={this.state.mobile}
                            />
                        </View>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => this.goOTPVerification()}>
                            <View style={styles.loginBtnView}>
                                <Text style={styles.loginBtnText}>Login / Signup &nbsp; <Icon name="long-arrow-right" color="#fff" size={18} /></Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: 20 }}>
                        <Text style={styles.continueText}>OR</Text>
                        <Text style={styles.continueText}>Continue with</Text>
                    </View>
                    <View style={styles.socialLoginParentView}>
                        <TouchableOpacity onPress={() => this.fbAuth()}>
                            <View style={styles.socialLoginBtnView}>
                                <Icon name="facebook-square" color="#435691" size={20} />
                                <Text style={styles.socialMediaText}>&nbsp;&nbsp;&nbsp;Facebook</Text>
                            </View>
                        </TouchableOpacity>
                        <Text>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                        <TouchableOpacity onPress={() => this.googleLogin()}>
                            <View style={styles.socialLoginBtnView}>
                                <Image
                                    source={require('../assets/images/google.png')}
                                    style={styles.googleIconImg}
                                />
                                <Text style={styles.socialMediaText}>&nbsp;&nbsp;&nbsp;Google</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.bottomSecurityNoteView}>
                        <Icon name="lock" size={15} color="#ccc" />
                        <Text style={{ fontSize: 12 }}>&nbsp;&nbsp;We'll never post anything without your permission.</Text>
                    </View>
                    <Text>{this.state.username}</Text>
                </View>
                <View>
                   
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#fff',
        height: '100%',
        padding: 15,
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    titleText: {
        fontWeight: 'bold',
        fontSize: 22,
        textAlign: 'center',
        marginTop: 120,
        marginBottom: 30,
    },
    inputParentView: {
        flexDirection: 'row',
        marginTop: 20,
        width: '100%'
    },
    mobileInputBox: {
        height: 40,
        borderColor: '#ccc',
        borderWidth: 1,
        paddingLeft: 10
    },
    loginBtnView: {
        backgroundColor: '#067c6c',
        height: 40,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15,
        borderRadius: 5,
    },
    loginBtnText: {
        color: '#fff',
        fontSize: 18,
        textAlign: 'center'
    },
    continueText: {
        color: '#474e56',
        fontSize: 18,
        marginTop: 20,
        textAlign: 'center'
    },
    socialLoginParentView: {
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    socialLoginBtnView: {
        flexDirection: 'row',
        height: 45,
        width: 150,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#ccc',
        borderWidth: 0.5
    },
    socialMediaText: {
        color: '#6e7caa',
        fontSize: 16,
        fontWeight: 'bold'
    },
    googleIconImg: {
        height: 20,
        width: 20,
    },
    bottomSecurityNoteView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 5
    },
});