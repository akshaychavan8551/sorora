import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, ScrollView, Alert } from 'react-native';
import StarRating from 'react-native-star-rating';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';

export default class ProfessionalDetails extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
            professionalData: [],
            profileReviews: [],
            subcategory_name: '',
            spinner: false,

            name: '',
            service: '',
            count: null,
            about: '',
            overview: '',
            profile: '',

            serviceId: '',

            username: '',
            email: '',
            usermobile: '',

            userID: '',

            // 
            provider_id: '',
            subcategory_id: '',
            category_id: '',



        }
    }
    componentDidMount() {
        this.getProfileData();
        this.getProfileReviews();
        this.getLoggedUserData();
    }

    getProfileData = () => {
        let name = JSON.stringify(this.props.navigation.getParam('name', 'None'))
        let sname = name.replace(/"/g, "");

        let service = JSON.stringify(this.props.navigation.getParam('service', 'None'))
        let pservice = service.replace(/"/g, "");

        let count = JSON.stringify(this.props.navigation.getParam('count', 'None'))
        let scount = count.replace(/"/g, "");

        let about = JSON.stringify(this.props.navigation.getParam('about', 'None'))
        let sabout = about.replace(/"/g, "");

        let overview = JSON.stringify(this.props.navigation.getParam('overview', 'None'))
        let soverview = overview.replace(/"/g, "");

        let profile = JSON.stringify(this.props.navigation.getParam('profile', 'None'))
        let sprofile = profile.replace(/"/g, "");

        // 
        let provider = JSON.stringify(this.props.navigation.getParam('provider', 'None'))
        let provider_id = provider.replace(/"/g, "");

        let subcatid = JSON.stringify(this.props.navigation.getParam('subCatID', 'None'))
        let subcategory_id = subcatid.replace(/"/g, "");

        let categoryid = JSON.stringify(this.props.navigation.getParam('category_id', 'None'))
        let category_id = categoryid.replace(/"/g, "");

        let service_id = JSON.stringify(this.props.navigation.getParam('s_id', 'None'))
        let s_id = service_id.replace(/"/g, "");



        this.setState({ name: sname })
        this.setState({ service: pservice })
        this.setState({ count: scount })
        this.setState({ about: sabout })
        this.setState({ overview: soverview })

        this.setState({ profile: sprofile })

        // 
        this.setState({ provider_id: provider_id })
        this.setState({ subcategory_id: subcategory_id })
        this.setState({ category_id: category_id })
        this.setState({ serviceId: s_id })

        console.log('prof details: ' + 'provider_id: ' + provider_id + 'subcategory_id: ' + subcategory_id + 'category_id: ' + category_id + ' serviceId ' + s_id)
    }
    getProfileReviews = () => {
        this.setState({
            spinner: !this.state.spinner
        });

        let provider = this.props.navigation.getParam('provider', 'None')
        // let sprovider = provider.replace(/"/g, "");

        let subCatID = this.props.navigation.getParam('subCatID', 'None')
        // let ssubCatID = subCatID.replace(/"/g, "");

        // let fd = new FormData();
        // fd.append('subcategory_id', ssubCatID);
        // fd.append('sprovider_id', sprovider);
        var apiHeader= {
            method: 'POST',
            headers: {
                // Accept: 'application/json', 
                // 'Content-Type': 'multipart/form-data'
                "Content-Type": "application/json",
                "Connection": "keep-alive",
                "Content-Type": "application/x-www-form-urlencoded",
                "accept-encoding": "gzip,deflate",
                "cache-control": "no-cache,no-cache"
            },
            body:"subcategory_id="+ subCatID+"&sprovider_id="+ provider,
        }
        fetch('https://omsoftware.org/sorora/api/getReviewByProffessionalIDandSubCategoryId', apiHeader).then((response) => response.json())
            .then((responseJson) => {
                this.setState({ profileReviews: responseJson.USERREVIEDETAILS, spinner: false });
            })
            .catch((error) => {
                this.setState({spinner: false })
                console.log(error)
            //    
            })
    }
    getLoggedUserData = async () => {
        const username = await AsyncStorage.getItem('user_name');
        const useremail = await AsyncStorage.getItem('user_email');
        const usermobile = await AsyncStorage.getItem('user_mobile');
        const userId = await AsyncStorage.getItem('userID');
        if (userId !== null) {
            this.setState({
                userID: userId
            })
        }
        if (username !== null) {
            this.setState({
                username: username
            })
        }
        if (useremail !== null) {
            this.setState({
                email: useremail
            })
        }
        if (usermobile !== null) {
            this.setState({
                usermobile: usermobile
            })
        }

    }
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                <View style={styles.headerView}>
                    <View style={styles.backBtnView}>
                        <TouchableOpacity style={{ height: '100%', width: '100%'}} onPress={() => this.props.navigation.goBack()}>
                            <Image
                                source={require('../assets/images/left_arrow.png')}
                                style={{ height: 25, width: 25 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.professionalTitleView}>
                        <Text style={styles.headerText}>Professional Details</Text>
                    </View>
                </View>
                <View style={styles.profileParentView}>
                    <View style={{ width: '20%', marginTop: 10 }}>
                        <View style={styles.userProfImgView}>
                            <Image
                                source={{ uri: this.state.profile }}
                                style={styles.profUserImg}
                            />
                            {/* <Icon name='user' size={25} color='#ccc' /> */}
                        </View>
                    </View>
                    <View style={{ width: '80%' }}>
                        <Text style={styles.userNameText}>{this.state.name}</Text>
                        <Text style={styles.serviceNameText}>{this.state.service}</Text>
                        <View style={styles.reviewParentView}>
                            <View style={{ width: '40%' }}>
                                <StarRating
                                    disabled={false}
                                    maxStars={5}
                                    rating={this.state.count}
                                    starSize={20}
                                    containerStyle={{ width: 25 }}
                                    starStyle={{ color: 'orange' }}
                                // selectedStar={(rating) => this.onStarRatingPress(rating)}
                                />
                            </View>
                            <View>
                                <Text>({this.state.count})</Text>
                            </View>
                        </View>
                        <View style={{ width: '100%' }}>
                            {(this.state.username != '' && this.state.useremail != '' && this.state.usermobile != '') ?
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Addresses', { user_id: this.state.userID, provider_id: this.state.provider_id, subcategory_id: this.state.subcategory_id, category_id: this.state.category_id, s_id: this.state.serviceId })}>
                                    <View style={styles.placeReqBtnView}>
                                        <Text style={styles.placeReqBtnText}>Place Request</Text>
                                    </View>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                                    <View style={styles.placeReqBtnView}>
                                        <Text style={styles.placeReqBtnText}>Place Request</Text>
                                    </View>
                                </TouchableOpacity>
                            }
                        </View>
                    </View>
                </View>
                <ScrollView style={{ height: '70%'}}>
                <View style={{ margin: 10 }}>
                    <View>
                        <Text style={styles.profileLabelText}>About</Text>
                        <View style={styles.profileInputBox}>
                            <Text>{this.state.about}</Text>
                        </View> 
                        {/* <TextInput
                            editable={false}
                            style={styles.profileInputBox}
                            value={this.state.about}
                            multiline={true}
                        /> */}
                    </View>
                    <View>
                        <Text style={styles.profileLabelText}>Overview</Text>
                        <View style={styles.profileOverInputBox}>
                            <Text>{this.state.overview}</Text>
                        </View>    
                        {/* <TextInput
                            editable={false}
                            style={styles.profileOverInputBox}
                            value={this.state.overview}
                            multiline={true}
                        /> */}
                    </View>
                    <View>
                        <Text style={styles.profileLabelText}>Reviews</Text>
                        <View style={styles.reviewView}>
                            {/* reviews */}
                            {this.state.profileReviews.map((data, index) => {
                                return (
                                    <View style={styles.reviewsBoxParentView}>
                                        <View style={styles.reviewsBoxView}>
                                            <View style={{ width: '20%', margin: 5 }}>
                                                <View style={styles.userProfImgView}>
                                                    <Image
                                                        source={{ uri: data.url }}
                                                        style={styles.profUserImg}
                                                    />
                                                </View>
                                            </View>
                                            <View style={{ width: '80%' }}>
                                                <Text style={styles.userNameText}>{data.user_name}</Text>
                                                <Text style={styles.serviceNameText}>{data.review_date}</Text>
                                                <View style={styles.reviewParentView}>
                                                    <View style={{ width: '40%' }}>
                                                        <StarRating
                                                            disabled={false}
                                                            maxStars={5}
                                                            rating={data.review}
                                                            starSize={20}
                                                            containerStyle={{ width: 25 }}
                                                            starStyle={{ color: 'orange' }}
                                                        // selectedStar={(rating) => this.onStarRatingPress(rating)}
                                                        />
                                                    </View>
                                                    <View>
                                                        <Text>({data.review})</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{ alignSelf: 'flex-start', marginTop: 10 }}>
                                            <Text style={{ fontWeight: 'bold' }}>{data.end_user_comment}</Text>
                                        </View>
                                    </View>
                                )
                            })
                            }
                            {/* end */}
                        </View>
                    </View>
                </View>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
    },
    backBtnView: {
        width: '20%',
        padding: 10,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    professionalTitleView: {
        width: '80%',
        paddingLeft: 20,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    profileParentView: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 10,
        padding: 10, margin: 5
    },
    userProfImgView: {
        height: 55,
        width: 55,
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    userNameText: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 5
    },
    serviceNameText: {
        fontSize: 14,
        marginBottom: 5
    },
    reviewParentView: {
        flexDirection: 'row',
        width: '100%'
    },
    placeReqBtnView: {
        backgroundColor: '#007a68',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
        width: 120,
        margin: 10,
        alignSelf: 'flex-end'
    },
    placeReqBtnText: {
        color: '#fff',
        fontSize: 14
    },
    profileLabelText: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    profileInputBox: {
        // height: 45,
        borderColor: '#ccc',
        borderWidth: 1,
        backgroundColor: '#fff',
        borderRadius: 10,
        marginTop: 5,
        padding: 10,
        color: '#000'
    },
    profileOverInputBox: {
        // height: 45,
        borderColor: '#ccc',
        borderWidth: 1,
        backgroundColor: '#fff',
        borderRadius: 10,
        marginTop: 5,
        padding: 10,
        color: '#000'
    },
    reviewView: {
        // height: 150,
        borderColor: '#ccc',
        borderWidth: 1,
        backgroundColor: '#fff',
        borderRadius: 10,
        marginTop: 5,
        padding: 5,
        paddingVertical: 20,
        color: '#000'
    },
    profUserImg: {
        height: 35,
        width: 35,
        alignSelf: 'center'
    },
    reviewsBoxParentView: {
        borderBottomColor: '#ccc',
        borderBottomWidth: 0.7,
        padding: 20,
        margin: 10
    },
    reviewsBoxView: {
        flexDirection: 'row',
        width: '100%',
    }
});