import React, { Component } from 'react';
import { createAppContainer, withNavigation } from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import OngoingBooking from './OngoingBooking'
import PastBooking from './PastBooking'
import BookService from './BookService'
import OngoingServiceDetails from './OngoingSeviceDetails'


const MessagesNav = createStackNavigator({
  OngoingBooking: { screen: OngoingBooking },
  OngoingServiceDetails: { screen: OngoingServiceDetails },
});

const AppTabNavigator = createMaterialTopTabNavigator({
  OngoingBooking: {
    screen: MessagesNav,
    navigationOptions: {
      tabBarLabel: 'Ongoing',
    }
  },
  PastBooking: {
    screen: PastBooking,
    navigationOptions: {
      tabBarLabel: 'History',
    }
  },
  BookService: {
    screen: BookService,
    navigationOptions: {
      tabBarLabel: 'Book Service',
    }
  },
}, {
  tabBarPosition: 'top',
  tabBarOptions: {
    tabStyle: {
      // borderColor: '#E2E2E2',
      // borderWidth: 0.7, 
    },
    activeTintColor: '#fff',
    inactiveTintColor: '#ccc',
    style: {
      height: 50,
      color: '#707070',
      backgroundColor: '#007a68',
    },
    indicatorStyle: {
      height: 50,
      backgroundColor: '#007a68',
      activeTintColor: '#007a68',
      borderBottomColor: 'red',
      borderBottomWidth: 2,
    },
    upperCaseLabel: true,
    showIcon: false,
    shifting: true,
    labelStyle: {
      fontWeight: 'bold',
      fontSize: 15,
    }
  },
});
class BookingTabBar extends Component {
  render() {
    return (
      <AppContainer />
    );
  }
}
const AppContainer = createAppContainer(AppTabNavigator);


export default withNavigation(BookingTabBar);
