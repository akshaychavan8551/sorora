import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';

export default class TypeList extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
            spinner: false,
            provider_id: '',
            subcategory_id: '',
            category_id: '',
            service_name: '',

            typeData: [],
            type_name: [],
            type_name_chk: [],
            // type: '',
            message: '',
        }
    }
    componentDidMount() {
        this.getTypeData();
    }
    getTypeData = async() => {
        const servName = await AsyncStorage.getItem('serviceName');
        this.setState({ service_name: servName})
        
        this.setState({
            spinner: !this.state.spinner
        });
      

        // let provider_ids = this.props.navigation.getParam('provider_id', 'NO-ID')
        // let provider_ids = providerid.replace(/"/g, "");
        // let subcategory_ids = this.props.navigation.getParam('subcategory_id', 'NO-ID')
        // let subcategory_ids = subcategoryid.replace(/"/g, "");
        // let category_ids = this.props.navigation.getParam('category_id', 'NO-ID')
        // let category_ids = category.replace(/"/g, "");
         
        const provider_ids = await AsyncStorage.getItem('provider_id');
        const subcategory_ids = await AsyncStorage.getItem('subcategory_id');
        const category_ids = await AsyncStorage.getItem('category_id');
        // let provider_id = JSON.stringify(provider_ids)
        // let subcategory_id = JSON.stringify(subcategory_ids)
        // let category_id = JSON.stringify(category_ids)
        // let fd = new FormData();
        // fd.append('subcategory_id', subcategory_ids);
        // fd.append('sprovider_id', provider_ids);
        // fd.append('category_id', category_ids);
        console.log('Service List : '+ 'provider_id: '+provider_ids + 'subcategory_id: '+subcategory_ids + 'category_id: '+category_ids)
        var apiHeader =  {
            method: 'POST',
            headers: {
                // Accept: 'application/json',
                "Content-Type": "application/json",
                "Connection": "keep-alive",
                "Content-Type": "application/x-www-form-urlencoded",
                "accept-encoding": "gzip,deflate",
                "cache-control": "no-cache,no-cache"
            },
            body: "subcategory_id="+subcategory_ids+"&sprovider_id="+provider_ids+"&category_id="+category_ids
        }
        fetch('https://omsoftware.org/sorora/api/getTypesBySpidandSubcatidandCatid',apiHeader
        ).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.RESPONSECODE == 0) {
                    this.setState({spinner: false})
                    let service_name = this.props.navigation.getParam('service_name', 'NO-ID')            
                    let service_price = this.props.navigation.getParam('service_price', 'NO-ID')
                    this.props.navigation.navigate('SelectDateTime',{service_name: service_name, service_price: service_price})

                    // this.goToNextProcess();
                    // Alert.alert(responseJson.RESPONSE, [
                    //     {text: 'OK', onPress: ()=> this.goToNextProcess()}
                    // ])
                } else if (responseJson.RESPONSECODE == 1) {
                    this.setState({ typeData: responseJson.TYPEDATA, spinner: false })
                    console.log(responseJson)

                } else {
                    this.setState({ message: responseJson, spinner: false})
                    console.log(responseJson)
                }
            })
            .catch((error) => {
                this.setState({spinner: false})
                 console.log(error)
                Alert.alert(
                    '',
                    'Server error. Please try again later.',
                    [
                        { text: 'OK' },
                    ]
                )
            }).done();
            // })
    }
    setTypesName = (name) => {
        // this.state.service_price_total.push(price)
        // console.log('name: '+name+ ',  Price:'+price)  
        if(this.state.type_name.includes(name + ' ') === false){
            var name_joined = this.state.type_name.concat(name + ' ');
           
            this.setState({ type_name: name_joined })
           
            this.setState({type_name_chk: name_joined});
        }     
        else if(this.state.type_name.includes(name + ' ') === true){
            for( var i = 0; i < this.state.type_name.length; i++){ 
                if ( this.state.type_name[i] === name + ' ') {
                    console.log(true)
                    this.state.type_name.splice(i, 1)                   
                }
                if(this.state.type_name_chk[i] === name + ' '){
                    var namenew = this.state.type_name_chk.splice(i, 1)
                }
             }
             var name_joined = this.state.type_name_chk.concat(namenew + ' ');
             this.setState({type_name_chk: name_joined})
        }else{ 
              
        } 
      
             
    }
    goToNextProcess = async() => {
        let providerid = JSON.stringify(this.props.navigation.getParam('provider_id', 'NO-ID'))
        let provider_id = providerid.replace(/"/g, "");

        let subcategoryid = JSON.stringify(this.props.navigation.getParam('subcategory_id', 'NO-ID'))
        let subcategory_id = subcategoryid.replace(/"/g, "");

        let category = JSON.stringify(this.props.navigation.getParam('category_id', 'NO-ID'))
        let category_id = category.replace(/"/g, "");


        let userId = JSON.stringify(this.props.navigation.getParam('user_id', 'NO-ID'))
        let user_id = userId.replace(/"/g, "");

        let address = JSON.stringify(this.props.navigation.getParam('address', 'NO-ID'))
        let user_address = address.replace(/"/g, "");

        let landmark = JSON.stringify(this.props.navigation.getParam('landmark', 'NO-ID'))
        let user_landmark = landmark.replace(/"/g, "");

        let service_name = this.props.navigation.getParam('service_name', 'NO-ID')
        // let service_name = serv_name.replace(/"/g, "");

        let service_price = this.props.navigation.getParam('service_price', 'NO-ID')
        // let service_price = serv_price.replace(/"/g, "");

        let brandname = JSON.stringify(this.props.navigation.getParam('brand_name', 'NO-ID'))
        let brand_name = brandname.replace(/"/g, "");

        let serviceid = JSON.stringify(this.props.navigation.getParam('s_id', 'NO-ID'))
        let s_id = serviceid.replace(/"/g, "");

        let toatamt = JSON.stringify(this.props.navigation.getParam('total', 'NO-ID'))
        let total = toatamt.replace(/"/g, "");

        if (this.state.type_name != '') {
            let typeName = JSON.stringify(this.state.type_name)
            // let typeNames = typeName.replace(/"/g, "");
            await AsyncStorage.setItem('type_name', typeName)
            this.props.navigation.navigate('SelectDateTime',{service_name: service_name, service_price: service_price})
            // this.props.navigation.navigate('SelectDateTime', { provider_id: provider_id, subcategory_id: subcategory_id, category_id: category_id, user_id: user_id, address: user_address, landmark: user_landmark, service_name: service_name, service_price: service_price, brand_name: brand_name, typename: this.state.type, s_id: s_id, total: total });
        } else {
            Alert.alert('Please select type')
        }

        console.log(' Type list page : provider_id ' + provider_id + 'subcategory_id ' + subcategory_id + 'category_id ' + category_id)
    }
    goBack = async() => {
        await AsyncStorage.removeItem('provider_id');
        await AsyncStorage.removeItem('subcategory_id');
        await AsyncStorage.removeItem('category_id');
        await AsyncStorage.removeItem('s_id')
        await AsyncStorage.removeItem('address')
        await AsyncStorage.removeItem('landmark')
        await AsyncStorage.removeItem('service_name');
        await AsyncStorage.removeItem('service_price');
        await AsyncStorage.removeItem('brand_name');
       await AsyncStorage.removeItem('typename');
       this.props.navigation.navigate('Addresses')
    }
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                 <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                <View style={styles.headerView}>
                    <View style={styles.backBtnView}>
                        <TouchableOpacity style={{ height: '100%', width: '100%'}} onPress={() => this.goBack()}>
                            <Image
                                source={require('../assets/images/left_arrow.png')}
                                style={{ height: 25, width: 25 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.professionalTitleView}>
                        <Text style={styles.headerText}>Select Type</Text>
                    </View>
                </View>
                <Text style={styles.serviceTitleLabel}>What type of {this.state.service_name}? </Text>
                {this.state.typeData.map((data, index) => {
                    return (
                            <View style={{ padding: 5, margin: 10, flexDirection: 'row', borderColor: '#ccc', borderWidth: 0.7 }}>
                                <View style={{ marginHorizontal: 12, width: '10%' }}>
                                    {(this.state.type_name_chk.includes(data.name + ' ') === true)  ?
                                        <TouchableOpacity onPress={() => this.setTypesName(data.name)}>
                                            <Icon name='check-square-o' color='green' size={30} />
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity onPress={() => this.setTypesName(data.name)}>
                                            <Icon name='square-o' color='#000' size={30} />
                                        </TouchableOpacity>
                                    }
                                </View>
                                <View style={{ width: '68%' }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{data.name}</Text>
                                </View>
                            </View>
                    )
                })
                }
                 {(this.state.typeData == null ) ?
                    <Text style={{ fontSize: 14, fontWeight: 'bold', textAlign: 'center' }}>No Data..</Text>
                    :
                    null
                }
                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center', marginBottom: 5 }}>
                    <TouchableOpacity onPress={() => this.goToNextProcess()}>
                        <View style={styles.nextBtnView}>
                            <Text style={styles.nextBtnText}>NEXT</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7',
        height: '100%',
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 5,
    },
    backBtnView: {
        width: '20%',
        padding: 10,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    professionalTitleView: {
        width: '80%',
        paddingLeft: 60,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    serviceTitleLabel: {
        fontSize: 16,
        fontWeight: 'bold',
        marginVertical: 20,
        textAlign: 'center',
    },
    nextBtnView: {
        backgroundColor: '#89cf34',
        height: 50,
        width: 150,
        alignItems: 'center',
        justifyContent: 'center'
    },
    nextBtnText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16
    }
})
