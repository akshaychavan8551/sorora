import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, Alert, Modal, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import PhoneInput from 'react-native-phone-input';
import ModalPickerImage from '../../ModalPickerImage'
import ImagePicker from 'react-native-image-picker';

class CountryCode extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);
        this.onPressFlag = this.onPressFlag.bind(this);
        this.selectCountry = this.selectCountry.bind(this);
        this.state = {
            pickerData: null,
            userCountryClass: '',
        }
    }
    async componentDidMount() {
        this.setState({
            pickerData: this.phone.getPickerData(),
        });
        const userCountry_Class01 = await AsyncStorage.getItem('userCountryClass');
        let userCountry_Class = userCountry_Class01;
        let num = userCountry_Class.match(/\d+/)
        console.log('slice: ' + num)
        this.setState({ userCountryClass: '+' + num })
        await AsyncStorage.setItem('userCountryClass', this.state.userCountryClass)

    }
    onPressFlag() {
        this.myCountryPicker.open();
    }

    async selectCountry(country) {
        console.log(this.state.pickerData)
        console.log(country.dialCode)
        this.phone.selectCountry(country.dialCode);
        this.setState({ userCountryClass: country.dialCode })
        // this.setState({countryClass: country.iso2})
        // this.setState({ countryTitle: country.label+': '+country.dialCode})
    }
    render() {
        return (
            <View>
                <PhoneInput
                    ref={(ref) => {
                        this.phone = ref;
                    }}
                    initialCountry={this.state.userCountryClass}
                    value={this.state.userCountryClass}
                    onPressFlag={this.onPressFlag}
                    style={{ paddingLeft: 10, color: '#000' }}
                />
                <ModalPickerImage
                    ref={(ref) => {
                        this.myCountryPicker = ref;
                        // console.log(ref)
                    }}
                    data={this.state.pickerData}
                    onChange={(country) => {
                        this.selectCountry(country);
                        // console.log(country.iso2)
                        // console.log(this.state.pickerData)
                    }}
                    cancelText="Cancel"
                />
            </View>
        )
    }
}
export default class CreateProfile extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);


        this.state = {
            // pickerData: null,
            modalVisible: false,
            // create
            mobile: '',
            dialCode: '',
            countryClass: '',
            countryTitle: '',
            name: '',
            email: '',
            ImageSource: null,
            filePath: null,

            // update
            userName: '',
            userMobile: '',
            userEmail: '',
            userID: '',
            userAddress: '',
            userGender: '0',
            title: 'Mr.',

            // set to local store
            setName: '',
            setContact: '',
            setEmail: '',
            setUserID: '',
            setUserAddress: '',
            setUserGender: '',
            setUserProfile: '',

           

        }
    }
    componentDidMount() {
        this.getData();
        this.getUpdateUserData();
    }


    getUpdateUserData = async () => {
        const username = await AsyncStorage.getItem('user_name');
        const useremail = await AsyncStorage.getItem('user_email');
        const usermobile = await AsyncStorage.getItem('user_mobile');
        const userId = await AsyncStorage.getItem('userID');
        const userAddress = await AsyncStorage.getItem('user_Address');
        const userGender = await AsyncStorage.getItem('user_Gender');
        const userProfile = await AsyncStorage.getItem('user_Profile');


        console.log('url: '+userProfile)

        if (username !== null) {
            this.setState({
                userName: username
            })
        }
        if (useremail !== null) {
            this.setState({
                userEmail: useremail
            })
        }
        if (usermobile !== null) {
            this.setState({
                userMobile: usermobile
            })
        }
        if (userId !== null) {
            this.setState({
                userID: userId
            })
        }
        if (userAddress !== null) {
            this.setState({
                userAddress: userAddress
            })
        }
        if (userProfile !== null) {
            this.setState({
                ImageSource: userProfile
            })
        }
        if (userGender == '0') {
            this.setState({
                title: 'Mr'
            })
        } else if (userGender == '1') {
            this.setState({
                title: 'Mrs'
            })
        } else if (userGender == '2') {
            this.setState({
                title: 'Miss'
            })
        } else if (userGender == '3') {
            this.setState({
                title: 'Master'
            })
        }
    }
    getData = () => {
        let mobile = JSON.stringify(this.props.navigation.getParam('mobile', ''))
        let mobilenum = mobile.replace(/"/g, "");
        this.setState({ mobile: mobilenum })

        // let dailCodenum = JSON.stringify(this.props.navigation.getParam('dailCode', ''))
        // let dailCode = dailCodenum.replace(/"/g, "");
        // this.setState({ dailCode: dailCode })
        let dailCodenum = JSON.stringify(this.props.navigation.getParam('dialCode', ''))
        let dailCode01 = dailCodenum.replace(/"/g, "");
        this.setState({ dialCode: dailCode01 })

        let countryClass00 = JSON.stringify(this.props.navigation.getParam('countryClass', ''))
        let countryClass01 = countryClass00.replace(/"/g, "");
        this.setState({ countryClass: countryClass01 })

        let username = JSON.stringify(this.props.navigation.getParam('name', ''))
        let usrname = username.replace(/"/g, "");
        this.setState({ name: usrname })

        let useremail = JSON.stringify(this.props.navigation.getParam('email', ''))
        let usremail = useremail.replace(/"/g, "");
        this.setState({ email: usremail })

        let countryTitle00 = JSON.stringify(this.props.navigation.getParam('countryTitle', ''))
        let countryTitle01 = countryTitle00.replace(/"/g, "");
        this.setState({ countryTitle: countryTitle01 })
    }


    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 70,
            maxHeight: 70,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            // console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                let img = JSON.stringify(source.uri)
                let image = img.replace(/"/g, "");
                let imgsource = response.data;
                console.log('image: ' + image)
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({

                    ImageSource: image,
                    filePath: imgsource,

                });
            }
        });
    }
    goCutomerAddress = () => {
             
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (this.state.name === '' || this.state.email === '' || this.state.mobile == '' && this.state.dailCode == '') {
            Alert.alert('Please fill all fields')
        } else if (reg.test(this.state.email) == false) {
            Alert.alert('email id not in format eg. test@gmail.com')
            return false;
        }else if(this.state.filePath === null ){
            Alert.alert('Please add profile picture.')
        } 
        else {
            console.log(this.state.dialCode)
            let fd = new FormData();
            fd.append('email_id', this.state.email);
            fetch('https://omsoftware.org/sorora/api/checkEmailMobileExist', {
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                body: fd
            }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.RESPONSECODE == 0) {
                        Alert.alert(responseJson.RESPONSE)
                    } else if (responseJson.RESPONSECODE == 1) {
                        this.props.navigation.navigate('CutomerAddress', { mobile: this.state.mobile, dialCode: this.state.dialCode, name: this.state.name, email: this.state.email, countryClass: this.state.countryClass, countryTitle: this.state.countryTitle, gender: this.state.userGender, userProfile: this.state.filePath });
                    } else {
                        console.log(responseJson)
                    }
                })
                .catch((error) => {
                    console.log(error)
                })

        }

    }
    setUserData = async () => {
        await AsyncStorage.setItem('user_name', this.state.setName)
        await AsyncStorage.setItem('user_email', this.state.setEmail)
        await AsyncStorage.setItem('user_mobile', this.state.setContact)
        await AsyncStorage.setItem('user_ID', this.state.setUserID)
        await AsyncStorage.setItem('user_Address', this.state.setUserAddress)
        await AsyncStorage.setItem('user_Gender', this.state.setUserGender)
        await AsyncStorage.setItem('user_Profile', this.state.setUserProfile)
    }
    goUpdateUserProfile = () => {
        let fd = new FormData();
        fd.append('user_name', this.state.userName);
        fd.append('user_email', this.state.userEmail);
        fd.append('gender', this.state.userGender);
        fd.append('user_contact', this.state.userMobile);
        fd.append('user_id', this.state.userID);
        fd.append('user_address', this.state.userAddress);
        if(this.state.filePath === null){
            fd.append('profile_picture', '')
        }else{
            fd.append('profile_picture', this.state.filePath)
        }
        
        fetch('https://omsoftware.org/sorora/api/updateUserProfile', {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: fd
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.RESPONSECODE == 0) {
                    Alert.alert(responseJson.RESPONSE)
                } else if (responseJson.RESPONSECODE == 1) {
                    this.setState({ setName: responseJson.USERDATA.user_name })
                    this.setState({ setEmail: responseJson.USERDATA.user_email })
                    this.setState({ setContact: responseJson.USERDATA.user_contact })
                    this.setState({ setUserProfile: responseJson.USERDATA.profile_url })
                    let id = JSON.stringify(responseJson.USERDATA.user_id)
                    this.setState({ setUserID: id })
                    let address = JSON.stringify(responseJson.USERDATA.user_service_address)
                    this.setState({ setUserAddress: address })
                    this.setState({ setUserGender: JSON.stringify(responseJson.USERDATA.gender) })
                    console.log(responseJson);
                    this.setUserData();
                    Alert.alert(responseJson.RESPONSE)
                    this.props.navigation.navigate('Profile');  

                } else {
                    console.log(responseJson)
                }

            })
            .catch((error) => {
                console.log(error)
                //     Alert.alert(
                //         '',
                //         'Unable to process this request. We apologize for any inconvenience. Please try again later.',
                //         [
                //             { text: 'OK' },
                //         ]
                //     )
                // }).done();
            })
    }

    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <View style={styles.headerView}>

                    <View style={styles.backBtnView}>
                        {
                            (this.state.userName != '' || this.state.userEmail != '') ?
                                <TouchableOpacity style={{ height: '100%', width: '100%' }} onPress={() => this.props.navigation.goBack()}>
                                    <Image
                                        source={require('../assets/images/left_arrow.png')}
                                        style={{ height: 25, width: 25 }}
                                    />
                                </TouchableOpacity>
                                :
                                (this.state.name == '' || this.state.email == '' || this.state.mobile == '') ?
                                    <TouchableOpacity style={{ height: '100%', width: '100%' }} onPress={() => alert('Please fill all fields and go next.')}>
                                        <Image
                                            source={require('../assets/images/left_arrow.png')}
                                            style={{ height: 25, width: 25 }}
                                        />
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity style={{ height: '100%', width: '100%' }} onPress={() => this.props.navigation.goBack()}>
                                        <Image
                                            source={require('../assets/images/left_arrow.png')}
                                            style={{ height: 25, width: 25 }}
                                        />
                                    </TouchableOpacity>
                        }
                    </View>
                    <View style={styles.professionalTitleView}>
                        {(this.state.userName != '' || this.state.userEmail != '') ?
                            <Text style={styles.headerText}>Update Profile</Text>
                            :
                            <Text style={styles.headerText}>Create Profile</Text>
                        }
                    </View>
                </View>
                <View style={styles.profileFormView}>
                    <View style={styles.imagePickerView}>
                        <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
                            {this.state.ImageSource === null ?
                                <View style={styles.pickImageView}>
                                    <Icon name='user-circle-o' color='#fff' size={60} />
                                </View>
                                :
                                <View style={styles.pickImageView}>
                                    <Image style={styles.ImageContainer} source={{ uri: this.state.ImageSource }} style={{ height: 65, width: 65, borderRadius: 30 }} />
                                </View>
                            }
                        </TouchableOpacity>
                    </View>
                </View>
                {(this.state.userName != '') ?
                    <View style={{ width: '100%', padding: 15 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ width: '25%' }}>
                                <TouchableOpacity onPress={() => this.setState({ modalVisible: true })}>
                                    <View style={{ backgroundColor: '#fff', flexDirection: 'row', height: 45, borderColor: '#ccc', borderWidth: 0.6, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ fontSize: 16, fontWeight: '500', width: '70%' }}>{this.state.title}</Text>
                                        <View style={{ alignItems: 'center', justifyContent: 'flex-end' }}>
                                            <Icon name='caret-down' size={20} color='#000' />
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: '75%' }}>
                                <TextInput
                                    placeholder='Name'
                                    style={styles.inputBox}
                                    onChangeText={(userName) => this.setState({ userName })}
                                    value={this.state.userName}
                                />
                            </View>
                        </View>
                        <TextInput
                            placeholder='Email ID'
                            autoCapitalize='none'
                            style={styles.inputBox}
                            onChangeText={(userEmail) => this.setState({ userEmail })}
                            keyboardType='email-address'
                            value={this.state.userEmail}
                        />
                    </View>
                    :
                    <View style={{ width: '100%', padding: 15 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ width: '25%' }}>
                                <TouchableOpacity onPress={() => this.setState({ modalVisible: true })}>
                                    <View style={{ backgroundColor: '#fff', flexDirection: 'row', height: 45, borderColor: '#ccc', borderWidth: 0.6, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ fontSize: 16, fontWeight: '500', width: '70%' }}>{this.state.title}</Text>
                                        <View style={{ alignItems: 'center', justifyContent: 'flex-end' }}>
                                            <Icon name='caret-down' size={20} color='#000' />
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: '75%' }}>
                                <TextInput
                                    placeholder='Name'
                                    style={styles.inputBox}
                                    onChangeText={(name) => this.setState({ name })}
                                    value={this.state.name}
                                />
                            </View>
                        </View>
                        <TextInput
                            placeholder='Email ID'
                            style={styles.inputBox}
                            autoCapitalize='none'
                            onChangeText={(email) => this.setState({ email })}
                            keyboardType='email-address'
                            value={this.state.email}
                        />
                    </View>
                }
                {(this.state.userName !== '') ?
                    <View style={{ flexDirection: 'row', width: '100%', paddingHorizontal: 15 }}>
                        <View style={{ width: '26%' }}>
                            {/* <View style={styles.dailcodeView}>
                                <Text>{this.state.dialCode}</Text>
                            </View> */}
                            <View style={styles.dailcodeView}>
                                <CountryCode />
                            </View>
                        </View>
                        <View style={{ width: '74%' }}>
                            <TextInput
                                placeholder='Contact Number '
                                // maxLength={10}
                                style={styles.inputBox}
                                onChangeText={(userMobile) => this.setState({ userMobile })}
                                value={this.state.userMobile}
                                keyboardType='phone-pad'
                            />
                        </View>
                    </View>
                    :
                    <View style={{ flexDirection: 'row', width: '100%', paddingHorizontal: 15 }}>
                        <View style={{ width: '20%' }}>
                            {/* <View style={styles.dailcodeView}> */}
                            <TextInput
                                editable={false}
                                placeholder='dial code'
                                style={styles.inputBox}
                                // onChangeText={(dialCode) => this.setState({ dialCode })}
                                value={this.state.dialCode}
                                keyboardType='phone-pad'
                            />
                            {/* </View> */}
                        </View>
                        <View style={{ width: '80%' }}>
                            {(this.state.mobile == '') ?
                                <TextInput
                                    placeholder='Contact Number '
                                    style={styles.inputBox}
                                    onChangeText={(mobile) => this.setState({ mobile })}
                                    value={this.state.mobile}
                                    keyboardType='phone-pad'
                                />
                                :
                                <TextInput
                                    editable={false}
                                    placeholder='Contact Number '
                                    style={styles.inputBox}
                                    onChangeText={(mobile) => this.setState({ mobile })}
                                    value={this.state.mobile}
                                    keyboardType='phone-pad'
                                />
                            }
                        </View>
                    </View>
                }
                <View>
                    {(this.state.userName !== '') ?
                        <TouchableOpacity onPress={() => this.goUpdateUserProfile()}>
                            <View style={styles.submitBtnView}>
                                <Text style={styles.submitBtnText}>Update</Text>
                            </View>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity onPress={() => this.goCutomerAddress()}>
                            <View style={styles.submitBtnView}>
                                <Text style={styles.submitBtnText}>Submit</Text>
                            </View>
                        </TouchableOpacity>
                    }
                </View>
                <View>
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => this.setState({ modalVisible: false })}
                        onRequestClose={() => {
                            // Alert.alert('Modal has been closed.');
                        }}>
                        <View style={styles.modalParentView}>
                            <View style={{ height: '60%', width: '80%', backgroundColor: '#fff', alignSelf: 'center', position: 'relative', top: '55%', margin: 15 }}>
                                <ScrollView style={{ height: '60%' }}>
                                    <View>
                                        <TouchableOpacity onPress={() => this.setState({ title: 'Mr', userGender: 0, modalVisible: false })}>
                                            <View style={{ alignItems: 'center', justifyContent: 'center', borderColor: '#ccc', borderWidth: 0.5, height: 30, margin: 5, }}>
                                                <Text style={{ fontSize: 14, fontWeight: 'normal' }}>Mr.</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({ title: 'Mrs', userGender: 1, modalVisible: false })}>
                                            <View style={{ alignItems: 'center', justifyContent: 'center', borderColor: '#ccc', borderWidth: 0.5, height: 30, margin: 5, }}>
                                                <Text style={{ fontSize: 14, fontWeight: 'normal' }}>Mrs.</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({ title: 'Miss', userGender: 2, modalVisible: false })}>
                                            <View style={{ alignItems: 'center', justifyContent: 'center', borderColor: '#ccc', borderWidth: 0.5, height: 30, margin: 5, }}>
                                                <Text style={{ fontSize: 14, fontWeight: 'normal' }}>Miss</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({ title: 'Master', userGender: 3, modalVisible: false })}>
                                            <View style={{ alignItems: 'center', justifyContent: 'center', borderColor: '#ccc', borderWidth: 0.5, height: 30, margin: 5, }}>
                                                <Text style={{ fontSize: 14, fontWeight: 'normal' }}>Master</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </ScrollView>
                                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center', marginBottom: 10, backgroundColor: '#f8f8f8', height: 40, elevation: 25 }}>
                                    <TouchableOpacity
                                        style={{ height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center' }}
                                        onPress={() => {
                                            this.setState({ modalVisible: false });
                                        }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 15, color: '#000', paddingBottom: 15 }}>Cancel</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7',
        height: '100%',
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
    },
    backBtnView: {
        width: '20%',
        padding: 10,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    professionalTitleView: {
        width: '80%',
        paddingLeft: 20,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    profileFormView: {
        marginTop: 5,
        padding: 15,
    },
    imagePickerView: {
        backgroundColor: '#007a68',
        height: 130,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    pickImageView: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 80,
        width: 80,
        borderColor: '#000',
        borderRadius: 40,
        borderWidth: 0.8,
    },
    inputBox: {
        backgroundColor: '#fff',
        height: 45,
        width: '100%',
        paddingLeft: 10,
        marginBottom: 10,
        borderRadius: 5,
        borderColor: '#ccc',
        borderWidth: 0.5,
        color: '#000'
    },
    dailcodeView: {
        height: 45,
        backgroundColor: '#fff',
        // alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#ccc',
        borderWidth: 0.5,
        marginRight: 3,
        borderRadius: 5,
    },
    submitBtnView: {
        backgroundColor: '#007a68',
        height: 50,
        width: 150,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        alignSelf: 'center'
    },
    submitBtnText: {
        fontSize: 16,
        color: '#fff',
    }
})