import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import Geolocation from '@react-native-community/geolocation';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button'

var radio_props = [
    { label: 'Male', value: 0 },
    { label: 'Female', value: 1 }
];

const googleApiKey = 'AIzaSyCRg0gXOIWJpvSCb43GzvCxJKwJU3Tg81o';

export default class CustomerAddress extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
            flat_building: '',
            landmark: '',
            locality: '',
            value: '0',

            // 
            name: '',
            email: '',
            dialCode: '',
            countryClass: '',
            countryTitle: '',
            mobile: '',
            userProfile: null,

            // when user is logged 
            loggedID: '',
            userAddress: '',
            userLandmark: '',
            userLocality: '',

            // for storage
            setName: '',
            setContact: '',
            setEmail: '',
            setUserID: '',
            setUserAddress: '',
            setUserProfile: '',

            locations: null

        }
    }
    componentDidMount() {
        this.getData();
        this.getId();

        Geolocation.getCurrentPosition(
            (position) => {
              this.getLocationDetails(position.coords.latitude,position.coords.longitude)
            },
            (error) => { console.log(error); },
            { enableHighAccuracy: false, enableLowAccuracy: true, timeout: 30000 }
          )
    }
    
    getLocationDetails(latitude, longitude) {
        let location = [];
        fetch('https://maps.googleapis.com/maps/api/geocode/json?address='+ latitude + ',' + longitude + '&key=' + googleApiKey)
        .then((response) => response.json())
        .then((responseJson) => {
            location = responseJson;
            this.setState({ userAddress: location.results[3].formatted_address})
            this.setState({ userLandmark: location.results[5].formatted_address})
            this.setState({ userLocality: location.results[5].formatted_address})

            this.setState({ flat_building: location.results[3].formatted_address})
            this.setState({ landmark: location.results[5].formatted_address})
            this.setState({ locality: location.results[5].formatted_address})
        //   console.log(responseJson)
        });
    }

    getId = async () => {
        const userId = await AsyncStorage.getItem('userID');
        this.setState({ loggedID: userId })
    }
    getData = () => {
        let mobile = JSON.stringify(this.props.navigation.getParam('mobile', ''))
        let mobilenum = mobile.replace(/"/g, "");
        this.setState({ mobile: mobilenum })

        let dailCodenum= JSON.stringify(this.props.navigation.getParam('dialCode', ''))
        let dailCode01 = dailCodenum.replace(/"/g, "");
        this.setState({ dialCode: dailCode01 })

        let countryClass00= JSON.stringify(this.props.navigation.getParam('countryClass', ''))
        let countryClass01 = countryClass00.replace(/"/g, "");
        this.setState({ countryClass: countryClass01 })

        let countryTitle00= JSON.stringify(this.props.navigation.getParam('countryTitle', ''))
        let countryTitle01 = countryTitle00.replace(/"/g, "");
        this.setState({ countryTitle: countryTitle01 })

        let name = JSON.stringify(this.props.navigation.getParam('name', ''))
        let username = name.replace(/"/g, "");
        this.setState({ name: username })

        let email = JSON.stringify(this.props.navigation.getParam('email', ''))
        let useremail = email.replace(/"/g, "");
        this.setState({ email: useremail })

        let gen = JSON.stringify(this.props.navigation.getParam('gender', ''))
        let usergender = gen.replace(/"/g, "");
        this.setState({ value: usergender })

        var profile = JSON.stringify(this.props.navigation.getParam('userProfile', ''))
        if(profile === null){
            console.log('url: '+profile)
            this.setState({ userProfile: '' })
        }else{
            let userprofile01 = profile.replace(/"/g, "");
            console.log('url: '+profile)
            this.setState({ userProfile: userprofile01 })
        }
       
        

        
    }
    setUserData = async () => {
        await AsyncStorage.setItem('user_name', this.state.setName)
        await AsyncStorage.setItem('user_email', this.state.setEmail)
        await AsyncStorage.setItem('user_mobile', this.state.setContact)
        await AsyncStorage.setItem('userID', this.state.setUserID)
        await AsyncStorage.setItem('user_Address', this.state.setUserAddress)
        await AsyncStorage.setItem('user_Gender', this.state.value)
        await AsyncStorage.setItem('user_Profile', this.state.setUserProfile)
    }

    registerOrLoginUser = () => {
        console.log('name : ' +this.state.name);  console.log('email: '+this.state.email);  console.log('mobile: '+this.state.mobile);  console.log('dailcode: '+this.state.dialCode);  
        console.log('flat_building: '+this.state.flat_building);  console.log('landmark: '+this.state.landmark);  console.log('locality: '+this.state.locality);  console.log('gender: '+this.state.value); 
        console.log('profile' +this.state.userProfile)
          
        let fd = new FormData();
        fd.append('name', this.state.name);
        fd.append('email_id', this.state.email);
        fd.append('gender', this.state.value);
        fd.append('initials', this.state.name);
        fd.append('mobile_no', this.state.mobile);
        fd.append('flat_building', this.state.flat_building);   
        fd.append('landmark', this.state.landmark);
        fd.append('locality', this.state.locality);
        fd.append('country_code', this.state.dialCode);
       
        if(this.state.countryClass === ''){
            fd.append('country_class','');
        }else{
            fd.append('country_class', this.state.countryClass);
        }
        if(this.state.userProfile === ''){
            fd.append('profile_picture','');
        }else{
            fd.append('profile_picture', this.state.userProfile);
        }
       
        fetch('https://omsoftware.org/sorora/api/registerUser', {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            body: fd
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.RESPONSECODE == 0) {
                    Alert.alert(responseJson.RESPONSE)
                } else if (responseJson.RESPONSECODE == 1) {
                    this.setState({ setName: responseJson.Data.user_name })
                    this.setState({ setEmail: responseJson.Data.user_email })
                    this.setState({ setContact: responseJson.Data.user_contact })
                    this.setState({ setUserProfile: responseJson.Data.profile_url })
                    let id = JSON.stringify(responseJson.Data.user_id)
                    this.setState({ setUserID: id })
                    let address = JSON.stringify(responseJson.Data.user_service_address)
                    this.setState({ setUserAddress: address })
                    console.log('setname: ', this.state.setName);
                    console.log(responseJson);
                    this.setUserData();
                    Alert.alert(responseJson.RESPONSE)
                    this.props.navigation.navigate('Search');

                } else {
                    console.log(responseJson)
                }
            })
            .catch((error) => {
                console.log(error)
            })
    }
    addAddress = () => {
        if(this.state.userAddress == '' || this.state.userLandmark == '' || this.state.userLocality == ''){
            Alert.alert('Please fill all address fields')
        }else{
            let fd = new FormData();
            fd.append('user_id', this.state.loggedID);
            fd.append('address', this.state.userAddress);
            fd.append('landmark', this.state.userLandmark);
            fd.append('locality', this.state.userLocality);

            fetch('https://omsoftware.org/sorora/api/userInsertAddres', {
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                body: fd
            }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.RESPONSECODE == 0) {
                        Alert.alert(responseJson.RESPONSE)
                    } else if (responseJson.RESPONSECODE == 1) {
                        Alert.alert(responseJson.RESPONSE)
                        this.props.navigation.navigate('Addresses');

                    } else {
                        console.log(responseJson)
                    }
                })
                .catch((error) => {
                    console.log(error)
                    Alert.alert(
                        '',
                        'Unable to process this request. We apologize for any inconvenience. Please try again later.',
                        [
                            { text: 'OK' },
                        ]
                    )
                }).done();
            }
    }
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <View style={styles.headerView}>
                    <View style={styles.backBtnView}>
                        <TouchableOpacity style={{ height: '100%', width: '100%'}} onPress={() => this.props.navigation.goBack()}>
                            <Image
                                source={require('../assets/images/left_arrow.png')}
                                style={{ height: 25, width: 25 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.professionalTitleView}>
                        {/* <Text style={styles.headerText}>Create Profile</Text> */}
                    </View>
                </View>
                <View style={styles.addressFormView}>
                    <Text style={styles.addressTitleText}>Where do you need the services ?</Text>
                    {(this.state.loggedID != null) ?
                        <View style={{ width: '100%', marginTop: 15 }}>
                            <TextInput
                                placeholder='Flat/Building/Street'
                                style={styles.inputBox}
                                onChangeText={(userAddress) => this.setState({ userAddress })}
                                value={this.state.userAddress}
                            />
                            <TextInput
                                placeholder='Landmark'
                                style={styles.inputBox}
                                onChangeText={(userLandmark) => this.setState({ userLandmark })}
                                value={this.state.userLandmark}
                            />
                            <TextInput
                                placeholder='Locality'
                                style={styles.inputBox}
                                onChangeText={(userLocality) => this.setState({ userLocality })}
                                value={this.state.userLocality}
                            />
                        </View>
                        :
                        <View style={{ width: '100%', marginTop: 15 }}>
                            <TextInput
                                placeholder='Flat/Building/Street'
                                style={styles.inputBox}
                                onChangeText={(flat_building) => this.setState({ flat_building })}
                                value={this.state.flat_building}
                            />
                            <TextInput
                                placeholder='Landmark'
                                style={styles.inputBox}
                                onChangeText={(landmark) => this.setState({ landmark })}
                                value={this.state.landmark}
                            />
                            <TextInput
                                placeholder='Locality'
                                style={styles.inputBox}
                                onChangeText={(locality) => this.setState({ locality })}
                                value={this.state.locality}
                            />
                            
                        </View>
                    }
                    {/* {(this.state.loggedID != null) ?
                        null
                        :
                        <View style={{ marginTop: 10 }}>
                            <RadioForm
                                radio_props={radio_props}
                                initial={0}
                                onPress={(value) => { this.setState({ value: value }) }}
                                formHorizontal={true}
                                labelHorizontal={true}
                                labelStyle={{ marginRight: 30 }}
                                buttonSize={15}
                            />
                            <Text>{this.state.value}</Text> 
                         </View>
                    }  */}
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                    {(this.state.loggedID != null) ?
                        <TouchableOpacity onPress={() => this.addAddress()}>
                            <View style={{ backgroundColor: '#89cf34', height: 45, alignItems: 'center', justifyContent: 'center', margin: 15 }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#fff' }}>Continue With This Address</Text>
                            </View>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity onPress={() => this.registerOrLoginUser()}>
                            <View style={{ backgroundColor: '#89cf34', height: 45, alignItems: 'center', justifyContent: 'center', margin: 15 }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#fff' }}>Continue With This Address</Text>
                            </View>
                        </TouchableOpacity>
                    }
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7',
        height: '100%',
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
    },
    backBtnView: {
        width: '20%',
        padding: 10,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    professionalTitleView: {
        width: '80%',
        paddingLeft: 20,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    addressFormView: {
        padding: 15,
        width: '100%'
    },
    addressTitleText: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    inputBox: {
        backgroundColor: '#fff',
        height: 45,
        // width: '100%',
        paddingLeft: 10,
        marginBottom: 10,
        borderRadius: 5,
        borderColor: '#ccc',
        borderWidth: 0.5
    },
});