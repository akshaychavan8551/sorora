import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, Alert, Modal, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';
import { WebView } from 'react-native-webview';

export default class TermsAndConditions extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
            spinner: false,
            country_id: '1',
            country_name: '',
            data: '',
            modalVisible: false,
            countryData: []
        }
    }
    componentDidMount() {
        this.getTermsConditionsData();
        this.getCountries();
    }
    getTermsConditionsData = () => {
        this.setState({
            spinner: !this.state.spinner
        });
        let fd = new FormData();
        fd.append('country_id', this.state.country_id);
        fetch('https://omsoftware.org/sorora/api/getTermConditionByCountryId', {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            body: fd
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.RESPONSECODE == 0) {
                    this.setState({ spinner: false, data: '' });
                    console.log(responseJson.RESPONSE)
                } else if (responseJson.RESPONSECODE == 1) {
                    this.setState({ spinner: false });
                    this.setState({ data: responseJson.RESPONSE.decription })
                    // console.log(responseJson.RESPONSE.decription)
                }

            }).catch(err => {
                // console.warn(err)
                Alert.alert(
                    '',
                    'Unable to process this request. We apologize for any inconvenience. Please try again later.',
                    [
                        { text: 'OK', onPress: () => this.setState({ spinner: false }) },
                    ]
                )
            }).done();
    }
    setTermsConditionData = (countryid, country_name) => {
        this.setState({ country_id: countryid, country_name: country_name})
        this.setState({ modalVisible: false })
        this.setState({
            spinner: !this.state.spinner
        });
        let fd = new FormData();
        fd.append('country_id', countryid);
        fetch('https://omsoftware.org/sorora/api/getTermConditionByCountryId', {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            body: fd
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.RESPONSECODE == 0) {
                    this.setState({ spinner: false, data: '' });
                    alert(responseJson.RESPONSE)
                } else if (responseJson.RESPONSECODE == 1) {
                    if(responseJson.RESPONSE.decription == ''){
                        this.setState({ spinner: false });
                        this.setState({ data: '' }) 
                    }else{
                        this.setState({ spinner: false });
                        this.setState({ data: responseJson.RESPONSE.decription })
                    }
                }

            }).catch(err => {
                // console.warn(err)
                Alert.alert(
                    '',
                    'Unable to process this request. We apologize for any inconvenience. Please try again later.',
                    [
                        { text: 'OK', onPress: () => this.setState({ spinner: false }) },
                    ]
                )
            }).done();
    }

    getCountries = () => {
        this.setState({
            spinner: !this.state.spinner
        });
        return fetch('https://omsoftware.org/sorora/api/getAllCountryList')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({ countryData: responseJson.DATA, spinner: false })
            }).catch(err => {
                // console.warn(err)
                Alert.alert(
                    '',
                    'Unable to process this request. We apologize for any inconvenience. Please try again later.',
                    [
                        { text: 'OK', onPress: () => this.setState({ spinner: false }) },
                    ]
                )
            }).done();
    }
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                <View style={styles.headerView}>
                    <View style={styles.backBtnView}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image
                                source={require('../assets/images/left_arrow.png')}
                                style={{ height: 25, width: 25 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.professionalTitleView}>
                        <Text style={styles.headerText}>Terms {'\&'} Conditions</Text>
                    </View>
                </View>
                <View style={{ height: '100%', width: '100%', marginBottom: 5 }}>
                    <View>
                        <TouchableOpacity onPress={() => this.setState({ modalVisible: true })}>
                            <View style={{ backgroundColor: '#fff', margin: 15, flexDirection: 'row', paddingLeft: 15, height: 40 }}>
                                <View style={{ width: '90%', justifyContent: 'center' }}>
                                    {(this.state.country_name == '') ?
                                        <Text style={{ fontSize: 14}}>Select Country</Text>
                                        :
                                        <Text style={{ fontSize: 14, fontWeight: 'bold'}}>{this.state.country_name}</Text>
                                    }

                                </View>
                                <View style={{ width: '10%', justifyContent: 'center' }}>
                                    <Icon name='caret-down' size={20} color='#000' />
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    {(this.state.data != null)? 
                    <WebView
                        style={{ marginHorizontal: 15, marginBottom: 5, padding: 10 }}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        scrollEnabled={true}
                        scalesPageToFit={false}
                        source={{ html: this.state.data }}
                    />
                    :
                    null
                    }
                </View>
                <View>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            Alert.alert('Modal has been closed.');
                        }}>
                        <View style={styles.modalParentView}>
                            <View style={{ height: '50%', width: '80%', backgroundColor: '#fff', alignSelf: 'center', position: 'relative', top: '20%', margin: 15 }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 16, marginVertical: 10, textAlign: 'center' }}>Select Country</Text>
                                <ScrollView style={{ height: '60%' }}>
                                    {this.state.countryData.map((data, index) => {
                                        return (
                                            <View>
                                                <TouchableOpacity onPress={() => this.setTermsConditionData(data.country_id, data.country_name)}>
                                                    <View style={{ alignItems: 'center', justifyContent: 'center', borderColor: '#ccc', borderWidth: 0.2, height: 30, margin: 5 }}>
                                                        <Text style={{ fontSize: 14, fontWeight: 'normal' }}>{data.country_name}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>

                                        )
                                    })
                                    }
                                </ScrollView>
                                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center', marginBottom: 10 }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({ modalVisible: false });
                                        }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 15, color: '#000' }}>Cancel</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7',
        height: '100%',
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        // marginLeft: 5,
    },
    backBtnView: {
        width: '20%',
        padding: 10,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    professionalTitleView: {
        width: '80%',
        paddingLeft: 40,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    modalParentView: {
        flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0.5)',
        position: "absolute",
    }
});