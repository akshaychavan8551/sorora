import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar } from 'react-native';
import BookingTabBar from './BookingTabBar'

export default class MyBookings extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
        }
    }
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <View style={styles.headerView}>
                    <Text style={styles.headerText}>My Bookings</Text>
                </View>
                <BookingTabBar />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7',
        height: '100%'
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
    },
});