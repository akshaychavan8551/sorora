import React, { Component } from 'react';
import { StyleSheet, StatusBar, View, Image } from 'react-native';
import NetInfo from "@react-native-community/netinfo";


export default class Splash extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(){
        super();
        this.state={}
        NetInfo.fetch().then(state => {
            // console.log("Connection type", state.type);
            // console.log("Is connected?", state.isConnected);
            if(state.isConnected == true){
                setInterval(() => {
                    this.props.navigation.navigate('Dashboard')
                },
                2000);
            }else{
                alert('You are offline....!\n\nPlease connect your device to network.');
                setInterval(() => {
                    NetInfo.fetch().then(state => {
                        if(state.isConnected == true){
                            this.props.navigation.navigate('Dashboard')
                        }
                    });
                }, 2000);
            }
        });
    }
    
    componentDidMount() {
        
    }
    render() {
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <Image
                    source={require('../assets/images/logo.png')}
                    style={styles.logoImg}
                />
            </View>
        )
    } s
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#000',
    },
    logoImg: {
        height: 250,
        width: 250,
    }
})
