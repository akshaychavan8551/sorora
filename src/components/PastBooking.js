import React, { Component } from 'react';
import { StyleSheet, StatusBar, View, Text, TouchableOpacity, Alert, ScrollView, TextInput } from 'react-native';
import { withNavigation } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import StarRating from 'react-native-star-rating';
import Dialog, { DialogTitle, DialogContent, DialogButton, DialogFooter, SlideAnimation, ScaleAnimation, } from 'react-native-popup-dialog';

export default class PastBooking extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);
        this.state = {
            pastServiceData: [],
            defaultAnimationDialog: false,
            comment: '',
            starCount: '',
            provider_id: '',
            service_id: '',
            subcategory_id: '',

        }
    }
    componentDidMount() {
        setInterval(() => this.getPastServices(), 4000);
    }
    getPastServices = async () => {
        let userId = await AsyncStorage.getItem('userID')
        if(userId == null){
            this.setState({ pastServiceData: []})
        }else{
            let fd = new FormData();
            fd.append('user_id', userId);
            fetch('https://omsoftware.org/sorora/api/historyOfUserBookings', {
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                body: fd
            }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.RESPONSECODE == 0) {
                        this.setState({ message: responseJson.RESPONSE })
                    } else if (responseJson.RESPONSECODE == 1) {
                        this.setState({ pastServiceData: responseJson.DATA })
                        // console.log(responseJson.DATA)
                    } else {
                        console.log(responseJson)
                    }
                })
                .catch((error) => {
                    console.log(error)
                })
        }         

    }
    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }
    sendReviewToProvider = async () => {
        console.log('user_id' + await AsyncStorage.getItem('userID')); console.log('review' + this.state.comment); console.log('s_id' + this.state.service_id);
        console.log('p_id' + this.state.provider_id); console.log('subcat_id' + this.state.subcategory_id); console.log('rating' + this.state.starCount)
        if (this.state.starCount == '' || this.state.comment == '') {
            Alert.alert('Please fill all feilds');
        } else {
            const userId = await AsyncStorage.getItem('userID');
            let fd = new FormData();
            fd.append('user_id', userId);
            fd.append('review', this.state.comment);
            fd.append('service_provider_id', this.state.provider_id);
            fd.append('service_id', this.state.service_id);
            fd.append('subcategory_id', this.state.subcategory_id);
            fd.append('rating', this.state.starCount);
            fetch('https://omsoftware.org/sorora/api/postReviewUserToServiceProvider', {
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                body: fd
            }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.RESPONSECODE == 0) {
                        Alert.alert(responseJson.RESPONSE)
                        this.setState({ defaultAnimationDialog: false })
                    } else if (responseJson.RESPONSECODE == 1) {
                        Alert.alert(responseJson.RESPONSE)
                        this.setState({ defaultAnimationDialog: false })
                    } else {
                        console.log(responseJson)
                    }
                })
                .catch((error) => {
                    console.log(error)
                })
        }
    }
    render() {
        return (
            <View style={{ height: '100%' }}>
                {(this.state.pastServiceData.length == 0) ?
                    null
                    :
                    <View style={{ marginVertical: 10, alignItems: 'center' }}>
                        <Text style={{ fontSize: 16, fontWeight: 'bold' }}>All History Booking Services</Text>
                    </View>
                }
                {(this.state.pastServiceData.length === 0) ?
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'center' }}>No Record Found..!</Text>
                    </View>
                    :
                    <ScrollView style={{ height: '60%' }}>
                        {this.state.pastServiceData.map((data, index) => {
                            return (
                                <View>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('OngoingServiceDetails', { booking_id: data.booking_id })}>
                                        <View style={{ margin: 5, padding: 10, borderRadius: 5, borderColor: '#ccc', borderWidth: 0.6 }}>
                                            <View style={{ flexDirection: 'row', width: '100%', marginVertical: 5 }}>
                                                <View style={{ width: '35%' }}>
                                                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Service Provider Name</Text>
                                                </View>
                                                <View style={{ width: '65%' }}>
                                                    <Text style={{ fontSize: 14, fontWeight: '500' }}>{data.sp_name}</Text>
                                                </View>
                                            </View>
                                            <View style={{ flexDirection: 'row', width: '100%', marginVertical: 2 }}>
                                                <View style={{ width: '35%' }}>
                                                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Service Name</Text>
                                                </View>
                                                <View style={{ width: '65%' }}>
                                                    <Text style={{ fontSize: 14, fontWeight: '500' }}>{data.booking_service_name}</Text>
                                                </View>
                                            </View>
                                            <View style={{ flexDirection: 'row', width: '100%', marginVertical: 2 }}>
                                                <View style={{ width: '35%' }}>
                                                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Description</Text>
                                                </View>
                                                <View style={{ width: '65%' }}>
                                                    <Text style={{ fontSize: 14, fontWeight: '500' }}>{data.s_description}</Text>
                                                </View>
                                            </View>
                                            <View style={{ flexDirection: 'row', width: '100%', marginVertical: 2 }}>
                                                <View style={{ width: '35%' }}>
                                                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Price</Text>
                                                </View>
                                                <View style={{ width: '65%' }}>
                                                    <Text style={{ fontSize: 14, fontWeight: '500' }}>{data.price}</Text>
                                                </View>
                                            </View>
                                            <View style={{ flexDirection: 'row', width: '100%', marginVertical: 2 }}>
                                                <View style={{ width: '35%' }}>
                                                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Status</Text>
                                                </View>
                                                <View style={{ width: '65%' }}>
                                                    {(data.booking_status === 1) ?
                                                        // <View style={{ backgroundColor: 'green', height: 20, width: 120, alignItems: 'center', justifyContent: 'center' }}>
                                                        <Text style={{ color: 'green', fontSize: 14, fontWeight: 'bold' }}>Completed</Text>
                                                        // </View>
                                                        :
                                                        (data.booking_status === 4 || data.booking_status === 3) ?
                                                            // <View style={{ backgroundColor: 'red', height: 20, width: 120, alignItems: 'center', justifyContent: 'center' }}>
                                                            <Text style={{ color: 'red', fontSize: 14, fontWeight: 'bold' }}>Canceled</Text>
                                                            // </View>
                                                            :
                                                            // <View style={{ backgroundColor: 'orange', height: 20, width: 120, alignItems: 'center', justifyContent: 'center' }}>
                                                            <Text style={{ color: 'red', fontSize: 14, fontWeight: 'bold' }}>Pending</Text>
                                                        // </View>
                                                    }
                                                </View>
                                            </View>
                                            {data.booking_status === 1 ?
                                                <View>
                                                    <TouchableOpacity onPress={() => this.setState({ defaultAnimationDialog: true, provider_id: data.provider_id, service_id: data.s_id, subcategory_id: data.subcategory_id })}>
                                                        <View style={{ backgroundColor: 'orange', height: 30, width: 60, alignItems: 'center', justifyContent: 'center', margin: 5, alignSelf: 'flex-end', borderRadius: 5 }}>
                                                            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 14 }}>Review</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                    <Dialog
                                                        onDismiss={() => {
                                                            this.setState({ defaultAnimationDialog: false });
                                                        }}
                                                        width={340}
                                                        visible={this.state.defaultAnimationDialog}
                                                        rounded
                                                        dialogTitle={
                                                            <DialogTitle
                                                                title="Review"
                                                                style={{
                                                                    backgroundColor: '#F7F7F8',
                                                                }}
                                                                hasTitleBar={false}
                                                                align="left"
                                                            />
                                                        }
                                                        footer={
                                                            <DialogFooter>
                                                                <DialogButton
                                                                    text="CANCEL"
                                                                    onPress={() => this.setState({ defaultAnimationDialog: false })}
                                                                />
                                                            </DialogFooter>
                                                        }
                                                    >
                                                        <DialogContent
                                                            style={{
                                                                backgroundColor: '#F7F7F8',
                                                                height: 300
                                                            }}
                                                        >
                                                            <View>
                                                                <View>
                                                                    <View>
                                                                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#ccc', marginBottom: 5 }}>Comment : </Text>
                                                                        <TextInput
                                                                            multiline={true}
                                                                            placeholder='comment'
                                                                            style={styles.inputBox}
                                                                            onChangeText={(comment) => this.setState({ comment })}
                                                                            value={this.state.comment}
                                                                        />
                                                                    </View>
                                                                    <View style={{ margin: 10 }}>
                                                                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#ccc', marginBottom: 5 }}>Rating : </Text>
                                                                        <StarRating
                                                                            disabled={false}
                                                                            emptyStar={'ios-star-outline'}
                                                                            fullStar={'ios-star'}
                                                                            halfStar={'ios-star-half'}
                                                                            iconSet={'Ionicons'}
                                                                            maxStars={5}
                                                                            rating={this.state.starCount}
                                                                            selectedStar={(rating) => this.onStarRatingPress(rating)}
                                                                            fullStarColor={'orange'}
                                                                        />
                                                                        <View style={{ alignSelf: 'flex-end' }}>
                                                                            <TouchableOpacity onPress={() => this.sendReviewToProvider()}>
                                                                                <View style={{ backgroundColor: '#007a68', height: 40, width: 150, alignItems: 'center', justifyContent: 'center', marginVertical: 15 }}>
                                                                                    <Text style={{ color: '#fff', fontSize: 16, fontWeight: 'bold' }}>Submit</Text>
                                                                                </View>
                                                                            </TouchableOpacity>
                                                                        </View>
                                                                    </View>
                                                                </View>
                                                            </View>
                                                        </DialogContent>
                                                    </Dialog>

                                                </View>
                                                :
                                                null
                                            }
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            )
                        })
                        }
                    </ScrollView>
                }
            </View>
        )
    }
}
const styles = StyleSheet.create({
    inputBox: {
        backgroundColor: '#fff',
        height: 100,
        width: '100%',
        paddingLeft: 10,
        marginBottom: 10,
        borderRadius: 5,
        borderColor: '#ccc',
        borderWidth: 0.5,
        color: '#000'
    },
})
