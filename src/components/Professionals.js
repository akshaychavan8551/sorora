import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, TextInput, Image, TouchableOpacity, ScrollView, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import StarRating from 'react-native-star-rating';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';

export default class Professionals extends Component {
    static navigationOptions = {
        header: null
    };
    constructor() {
        super();
        this.state = {
            professionalData: [],
            subcategory_name: '',
            spinner: false,
        }
    }
    componentDidMount() {
        this.getProfessionalData();
    }
    getProfessionalData = () => {
        this.setState({
            spinner: !this.state.spinner
        });
        let subCatID = JSON.stringify(this.props.navigation.getParam('subcategory_id', 'NO-ID'))
        let subCatName = JSON.stringify(this.props.navigation.getParam('subcategory_name', 'NO-SUB-CATEGORY'))
        let name = subCatName.replace(/"/g, "");
        this.setState({ subcategory_name: name })
        // let fd = new FormData();
        // fd.append('subcategory_id', subCatID);
        var apiHeader= {
            method: 'POST',
            headers: {
                // Accept: 'application/json', 
                "Content-Type": "application/json",
                "Connection": "keep-alive",
                "Content-Type": "application/x-www-form-urlencoded",
                "accept-encoding": "gzip,deflate",
                "cache-control": "no-cache,no-cache"
            },
            body:"subcategory_id="+ subCatID
        }
        fetch('https://omsoftware.org/sorora/api/getProffessionalBySubCategoryId', apiHeader
        ).then((response) => response.json())
            .then((responseJson) => {
                this.setState({ professionalData: responseJson.PROFFESSIONAL_DETAILS, spinner: false });
            })
            .catch((error) => {
                this.setState({spinner: false })
                console.log(error)
            //     Alert.alert(
            //         '',
            //         'Unable to process this request. We apologize for any inconvenience. Please try again later.',
            //         [
            //             { text: 'OK', onPress: () => this.setState({ spinner: false }) },
            //         ]
            //     )
            // }).done();
            })
    }
    goProfessionalDetails = async(name, count, about, overview, profile, provider, category_id, s_id) => {
        let subCatID = JSON.stringify(this.props.navigation.getParam('subcategory_id', 'NO-ID'))
        let service = JSON.stringify(this.props.navigation.getParam('subcategory_name', 'NO-SUB-CATEGORY'))
        let serviceName = service.replace(/"/g, "");
        await AsyncStorage.setItem('serviceName', serviceName)
        this.props.navigation.navigate('ProfessionalDetails', {
            name: name,
            service: serviceName,
            count: count,
            about: about,
            overview: overview,
            profile: profile,
            provider: provider,
            subCatID: subCatID,
            category_id: category_id,
            s_id: s_id
        })
        console.log('prof provider :' + provider)
        console.log('prof s id :' + subCatID)
    }
    goBack = () => {
        this.setState({ subcategory_name: '', professionalData: [] })
        this.props.navigation.goBack()
    }
    render() {
        let professionalData = this.state.professionalData.map((data, index) => {
            return (
                <View>
                    <View style={{ flexDirection: 'row', backgroundColor: '#fff', borderWidth: 1, borderColor: '#ccc', borderRadius: 10, padding: 10, margin: 5 }}>
                        <View style={{ width: '20%', justifyContent: 'center' }}>
                            <View style={{ height: 55, width: 55, borderWidth: 1, borderColor: '#ccc', borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity onPress={() => this.goProfessionalDetails(data.sp_name, data.average_review, data.sp_about, data.sp_overview, data.url, data.provider_id, data.category_id, data.s_id)}>
                                    <Image
                                        source={{ uri: data.url }}
                                        style={styles.profUserImg}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ width: '60%' }}>
                            <TouchableOpacity onPress={() => this.goProfessionalDetails(data.sp_name, data.average_review, data.sp_about, data.sp_overview, data.url, data.provider_id, data.category_id, data.s_id)}>
                                <Text style={{ fontSize: 16, fontWeight: 'bold', marginBottom: 5 }}>{data.sp_name}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.goProfessionalDetails(data.sp_name, data.average_review, data.sp_about, data.sp_overview, data.url, data.provider_id, data.category_id, data.s_id)}>
                                <Text style={{ fontSize: 14, marginBottom: 5 }}>{this.state.subcategory_name}</Text>
                            </TouchableOpacity>
                            <View style={{ flexDirection: 'row', width: '100%' }}>
                                <View style={{ width: '55%' }}>
                                    <StarRating
                                        disabled={false}
                                        maxStars={5}
                                        rating={data.average_review}
                                        starSize={20}
                                        containerStyle={{ width: 25 }}
                                        starStyle={{ color: 'orange' }}
                                    // selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    />
                                </View>
                                <View>
                                    <Text>({data.average_review})</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            )
        })
        return (
            <View style={styles.mainView}>
                <StatusBar
                    backgroundColor="#067c6c"
                    barStyle="light-content"
                />
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                <View style={styles.headerView}>
                    <View style={styles.backBtnView}>
                        <TouchableOpacity style={{ height: '100%', width: '100%'}} onPress={() => this.goBack()}>
                            <Image
                                source={require('../assets/images/left_arrow.png')}
                                style={{ height: 25, width: 25 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.professionalTitleView}>
                        <Text style={styles.headerText}>Professional</Text>
                    </View>
                </View>
                <ScrollView style={{ height: '70%'}}>
                    {(this.state.professionalData.length > 0) ?
                        professionalData
                        :
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#ccc' }}>No Data Found..</Text>
                        </View>
                    }
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#f7f7f7'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    headerView: {
        height: 50,
        width: '100%',
        backgroundColor: '#007a68',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },
    headerText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
    },
    backBtnView: {
        width: '20%',
        padding: 10,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    professionalTitleView: {
        width: '80%',
        paddingLeft: 50,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    profUserImg: {
        height: 35,
        width: 35,
        alignSelf: 'center'
    },
});